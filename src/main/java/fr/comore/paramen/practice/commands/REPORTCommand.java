package fr.comore.paramen.practice.commands;

import fr.comore.paramen.rank.accounts.Account;
import fr.comore.paramen.rank.ranks.RankUnit;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class REPORTCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;

            if(strings.length >= 2) {
                if(Bukkit.getPlayer(strings[0]) == null) {
                    player.sendMessage("§dThis player is not online.");
                    return false;
                }

                Player targetPlayer = Bukkit.getPlayer(strings[0]);

                if(player == targetPlayer) {
                    player.sendMessage("§dYou can't report yourself.");
                    return false;
                }

                StringBuilder stringBuilder = new StringBuilder();
                for(String string : strings) {
                    if(string != strings[0]) {
                        stringBuilder.append("§d" + string + " ");
                    }
                }

                TextComponent textComponent = new TextComponent("§7[§dReport§7] §d" + player.getName() + "§d has reported §d" + targetPlayer.getName()+" for §6" + stringBuilder.toString());

                for(Player players : Bukkit.getServer().getOnlinePlayers()) {
                    if(Account.getAccount(players).getRank().getPower() >= RankUnit.MOD.getPower()) {
                        players.spigot().sendMessage(textComponent);
                    }
                }
                player.sendMessage("§dYour report has been sended to the connected staff members.");
            } else {
                player.sendMessage("§dUsage: /report <player> <message>");
            }
        }

        return false;
    }
}
