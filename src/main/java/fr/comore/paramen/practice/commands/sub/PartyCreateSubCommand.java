package fr.comore.paramen.practice.commands.sub;

import fr.comore.paramen.practice.party.PartyManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import org.bukkit.entity.Player;

public class PartyCreateSubCommand extends PartySubCommand {

    String[] args;
    Player player;
    public PartyCreateSubCommand(String[] args, Player player) {
        this.args = args;
        this.player = player;
    }

    @Override
    public String command() {
        return "party";
    }

    @Override
    public String helpDescription() {
        return "Creates a party";
    }

    @Override
    public String[] args() {
        return args;
    }

    @Override
    public void execute() {
        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("create")) {
                PartyManager.createParty(PracticePlayer.getAccount(player));
            }
        }
    }
}
