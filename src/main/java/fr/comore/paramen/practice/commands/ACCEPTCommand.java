package fr.comore.paramen.practice.commands;

import fr.comore.paramen.practice.DuelManager;
import fr.comore.paramen.practice.kits.KitManager;
import fr.comore.paramen.practice.matchs.MatchManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.rank.accounts.Account;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ACCEPTCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            PracticePlayer practicePlayer = PracticePlayer.getAccount(player);

            if(strings.length == 1) {
                if(Bukkit.getPlayer(strings[0]) == null) {
                    player.sendMessage("§dError: This player isn't online.");
                    return false;
                }

                if(KitManager.editors.containsKey(PracticePlayer.getAccount(player))) {
                    player.sendMessage("§dError: You're editing a kit.");
                    return false;
                }

                if(PracticePlayer.getAccount(player).isInMatch()) {
                    player.sendMessage("§dError: You're in match.");
                    return false;
                }

                if(KitManager.editors.containsKey(PracticePlayer.getAccount(Bukkit.getPlayer(strings[0])))) {
                    player.sendMessage("§dError: The player is editing a kit.");
                    return false;
                }

                if(PracticePlayer.getAccount(Bukkit.getPlayer(strings[0])).isInMatch()) {
                    player.sendMessage("§dError: The player in match.");
                    return false;
                }

                Player targetPlayer = Bukkit.getPlayer(strings[0]);
                PracticePlayer targetPracticePlayer = PracticePlayer.getAccount(targetPlayer);

                if(DuelManager.isDueled(targetPracticePlayer)) {
                    if(DuelManager.getDueler(targetPracticePlayer).getPlayer() == practicePlayer.getPlayer()) {
                        if(DuelManager.getLadder(targetPracticePlayer) != null) {
                            MatchManager.addUnrankedMatch(DuelManager.getLadder(targetPracticePlayer), targetPracticePlayer, practicePlayer);
                        }
                    } else {
                        player.sendMessage("§dThis player hasn't dueled you.");
                    }
                }

            } else {
                player.sendMessage("§dUsage: /accept <player>");
            }
        }

        return false;
    }
}
