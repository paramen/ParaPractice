package fr.comore.paramen.practice.commands.sub;

public abstract class SubCommand {

    public abstract String command();
    public abstract String[] args();
    public abstract void execute();

}
