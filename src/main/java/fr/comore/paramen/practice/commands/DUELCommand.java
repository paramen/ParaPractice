package fr.comore.paramen.practice.commands;

import fr.comore.paramen.practice.DuelManager;
import fr.comore.paramen.practice.kits.KitManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DUELCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            PracticePlayer practicePlayer = PracticePlayer.getAccount((Player) commandSender);
            if(strings.length == 1) {
                if(Bukkit.getPlayer(strings[0]) == null) {
                    practicePlayer.getPlayer().sendMessage("§dError: This player isn't online.");
                    return false;
                }



                if(KitManager.editors.containsKey(PracticePlayer.getAccount(player))) {
                    player.sendMessage("§dError: You're editing a kit.");
                    return false;
                }

                if(PracticePlayer.getAccount(player).isInMatch()) {
                    player.sendMessage("§dError: You're in match.");
                    return false;
                }

                if(KitManager.editors.containsKey(PracticePlayer.getAccount(Bukkit.getPlayer(strings[0])))) {
                    player.sendMessage("§dError: The player is editing a kit.");
                    return false;
                }

                if(PracticePlayer.getAccount(Bukkit.getPlayer(strings[0])).isInMatch()) {
                    player.sendMessage("§dError: The player in match.");
                    return false;
                }

                PracticePlayer targetPracticePlayer = PracticePlayer.getAccount(Bukkit.getPlayer(strings[0]));

                if(practicePlayer != targetPracticePlayer) {
                    practicePlayer.openDuelInventory(targetPracticePlayer);
                } else {
                    practicePlayer.getPlayer().sendMessage("§dError: You can't duel yourself.");
                }
            } else {
                commandSender.sendMessage("§dUsage: /duel <player>§d: Duel a player.");
            }
        }

        return false;
    }
}
