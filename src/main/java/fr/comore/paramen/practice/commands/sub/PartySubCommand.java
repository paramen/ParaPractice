package fr.comore.paramen.practice.commands.sub;

public abstract class PartySubCommand {

    public abstract String command();
    public abstract String helpDescription();
    public abstract String[] args();
    public abstract void execute();

}
