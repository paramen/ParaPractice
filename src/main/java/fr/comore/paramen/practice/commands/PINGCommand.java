package fr.comore.paramen.practice.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class PINGCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            CraftPlayer craftPlayer = (CraftPlayer) player;

            player.sendMessage("§dYour ping: §6"+craftPlayer.getHandle().ping+"ms");
        }

        return false;
    }
}
