package fr.comore.paramen.practice.commands;

import fr.comore.paramen.practice.inventory.InventoryManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class INVCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            PracticePlayer practicePlayer = PracticePlayer.getAccount(player);

            if(strings.length == 1) {
                if(Bukkit.getPlayer(strings[0]) == null) {
                    player.sendMessage("§dThis player is not online.");
                    return false;
                }

                if(InventoryManager.getInventories().containsKey(PracticePlayer.getAccount(Bukkit.getPlayer(strings[0])))) {
                    Inventory inventory = Bukkit.createInventory(null, 5*9, "§dInventory of " + Bukkit.getPlayer(strings[0]).getName());
                    inventory.setContents(InventoryManager.getInventories().get(PracticePlayer.getAccount(Bukkit.getPlayer(strings[0]))));
                    player.openInventory(inventory);
                }
            } else {
                player.sendMessage("§dUsage: /"+s+" <player>");
            }
        }

        return false;
    }
}
