package fr.comore.paramen.practice.commands;

import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.rank.accounts.Account;
import fr.comore.paramen.rank.ranks.RankUnit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameModeCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            PracticePlayer practicePlayer = PracticePlayer.getAccount(player);
            Account account = Account.getAccount(player);

            if((account.getRank().getPower() >= RankUnit.ADMIN.getPower() || account.getRank() == RankUnit.BUILDER) && (!(practicePlayer.isInMatch() || practicePlayer.isInQueue()))) {
                if(strings.length == 1) {
                    if(strings[0].equalsIgnoreCase("s") || strings[0].equalsIgnoreCase("0")) {
                        player.setGameMode(GameMode.SURVIVAL);
                        player.sendMessage("§dYour GameMode has been updated to "+GameMode.SURVIVAL+".");
                    } else if(strings[0].equalsIgnoreCase("c") || strings[0].equalsIgnoreCase("1")) {
                        player.setGameMode(GameMode.CREATIVE);
                        player.sendMessage("§dYour GameMode has been updated to "+GameMode.CREATIVE+".");
                    } else if(strings[0].equalsIgnoreCase("a") || strings[0].equalsIgnoreCase("2")) {
                        player.setGameMode(GameMode.ADVENTURE);
                        player.sendMessage("§dYour GameMode has been updated to "+GameMode.ADVENTURE+".");
                    }
                } else {
                    commandSender.sendMessage("§dUsage: /"+command.getName() + " <s/c/0/1>");
                }
            }
        }

        return false;
    }
}
