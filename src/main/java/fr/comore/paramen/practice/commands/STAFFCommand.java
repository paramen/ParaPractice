package fr.comore.paramen.practice.commands;

import fr.comore.paramen.rank.accounts.Account;
import fr.comore.paramen.rank.ranks.RankUnit;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class STAFFCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;

            if(Account.getAccount(player).getRank().getPower() < RankUnit.BUILDER.getPower()) {
                player.sendMessage("§dError: You don't have necessary permissions to perform this command.");
                return false;
            }

            if(strings.length >= 1) {
                StringBuilder stringBuilder = new StringBuilder();
                for(String string : strings) {
                    stringBuilder.append(string + " ");
                }

                for(Player players : Bukkit.getServer().getOnlinePlayers()) {
                    if(Account.getAccount(player).getRank().getPower() >= RankUnit.BUILDER.getPower()) players.sendMessage("§d[Staff] §6" + Account.getAccount(player).getRank().getPrefix()+player.getName()+"§8: §f" + stringBuilder.toString());
                }
            } else {
                player.sendMessage("§dUsage: /" + s + " <message>");
            }
        }

        return false;
    }
}
