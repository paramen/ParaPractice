package fr.comore.paramen.practice.commands;

import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.rank.accounts.Account;
import fr.comore.paramen.rank.accounts.OffineAccount;
import fr.comore.paramen.rank.ranks.RankUnit;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RankCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            if(Account.getAccount((Player) commandSender).getRank().getPower() < RankUnit.ADMIN.getPower()) {
                commandSender.sendMessage("§dError: You don't have the necessary permissions.");
                return true;
            }
        }
        if(strings.length == 2) {
            if(Bukkit.getOfflinePlayer(strings[0]).getUniqueId() == null) {
                commandSender.sendMessage("§dError: This player hasn't played before on the server.");
                return true;
            }

            if(RankUnit.getByName(strings[1]) == null) {
                commandSender.sendMessage("§dError: This rank doesn't exist.");
                return true;
            }

            RankUnit rankUnit = RankUnit.getByName(strings[1]);
            String name = "";

            if(commandSender instanceof Player && Account.getAccount((Player) commandSender).getRank().getPower() <= rankUnit.getPower()) {
                commandSender.sendMessage("§dYou can't assign this rank because you have an equal or lower rank.");
                return false;
            } else {
                if(Bukkit.getOfflinePlayer(strings[0]).isOnline()) {
                    Account account = Account.getAccount(Bukkit.getPlayer(strings[0]));
                    if(commandSender instanceof Player && Account.getAccount((Player) commandSender).getRank().getPower() <= account.getRank().getPower()) {
                        commandSender.sendMessage("§dYou can't assign this rank because you have an equal or lower rank.");
                        return false;
                    }
                    if(account.getRank() != rankUnit) {
                        account.setRank(rankUnit);
                        if(PracticePlayer.getAccount(Bukkit.getPlayer(strings[0])).isInMatch()) {

                            ParaPractice.scoreboard.get(PracticePlayer.getAccount(Bukkit.getPlayer(strings[0])).getPlayer().getUniqueId()).setMatchScoreboard();
                            ParaPractice.scoreboard.get(PracticePlayer.getAccount(Bukkit.getPlayer(strings[0])).getPlayer().getUniqueId()).setTag();
                        } else {
                            ParaPractice.scoreboard.get(PracticePlayer.getAccount(Bukkit.getPlayer(strings[0])).getPlayer().getUniqueId()).refresh();
                        }
                        Bukkit.getPlayer(strings[0]).sendMessage("§dYou've been awarded the " + rankUnit.getNameColor() + " §drank.");

                        name = Bukkit.getPlayer(strings[0]).getName();
                    } else {
                        commandSender.sendMessage("§dThis player has already this rank.");
                    }
                } else {
                    OffineAccount account = new OffineAccount(Bukkit.getOfflinePlayer(strings[0]).getUniqueId());
                    if(commandSender instanceof Player && Account.getAccount((Player) commandSender).getRank().getPower() <= account.getRank().getPower()) {
                        commandSender.sendMessage("§dYou can't assign this rank because you have an equal or lower rank.");
                        return false;
                    }
                    if(account.getRank() != rankUnit) {
                        account.setRank(rankUnit);
                        name = Bukkit.getOfflinePlayer(strings[0]).getName();
                    } else {
                        commandSender.sendMessage("§dThis player has already this rank.");
                    }
                }
            }

            if(!name.equalsIgnoreCase("")) { commandSender.sendMessage("§dThe "+rankUnit.getNameColor()+" §drank has been awarded to §8"+name+"§d."); }
        } else {
            if(strings.length == 1 && strings[0].equalsIgnoreCase("list")){
                commandSender.sendMessage("§dList of ranks: ");
                for(RankUnit rankUnit : RankUnit.values()) {
                    commandSender.sendMessage("§d- §8"+rankUnit.getNameColor() + " §d: §dPREFIX: §8"+rankUnit.getPrefix() + " §d: §d"+rankUnit.getPower());
                }
                return true;
            }

            commandSender.sendMessage("§dUsage: /rank <player> <rank>");
        }

        return false;
    }
}
