package fr.comore.paramen.practice.commands;

import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.spectator.SpectatorManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SPECCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            PracticePlayer practicePlayer = PracticePlayer.getAccount(player);

            if(strings.length == 1) {
                if(Bukkit.getPlayer(strings[0]) != null) {
                    if(PracticePlayer.getAccount(Bukkit.getPlayer(strings[0])).isInMatch() && !practicePlayer.isInMatch() &&  !practicePlayer.isInQueue()) {
                        SpectatorManager.addSpectator(practicePlayer, PracticePlayer.getAccount(Bukkit.getPlayer(strings[0])));
                    } else {
                        player.sendMessage("§dThis player is not in match.");
                    }
                } else {
                    player.sendMessage("§dThis player is not online.");
                }
            } else {
                player.sendMessage("§dUsage: /spec <player>");
            }
        }
        return false;
    }
}
