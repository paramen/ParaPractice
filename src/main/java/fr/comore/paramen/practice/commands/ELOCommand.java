package fr.comore.paramen.practice.commands;

import fr.comore.paramen.practice.elos.ELOManager;
import fr.comore.paramen.practice.ladders.Ladder;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.utils.ItemBuilder;
import fr.comore.paramen.rank.accounts.Account;
import fr.comore.paramen.rank.accounts.OffineAccount;
import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.PacketPlayOutPlayerInfo;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.UUID;

public class ELOCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;

            if(PracticePlayer.getAccount(player).isInMatch()) {
                player.sendMessage("§eYou can't do this in your status.");
                return false;
            }

            if(strings.length == 1) {
                if(Bukkit.getOfflinePlayer(strings[0]).hasPlayedBefore()) {
                    OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(strings[0]);

                    Inventory inventory = Bukkit.createInventory(null, 2*9, "§d"+offlinePlayer.getName()+"§e's stats");
                    inventory.addItem(new ItemBuilder(Material.SUGAR).setName("§a"+"Global").setLore("§eElo: §d"+ELOManager.getGlobalElo(offlinePlayer.getUniqueId())).toItemStack());
                    for(Ladder ladder : Ladder.values()) {
                        inventory.addItem(new ItemBuilder(ladder.getItemStack()).setName("§a"+ladder.getName()).setLore("§eElo: §d"+ELOManager.getLadderElo(ladder, offlinePlayer.getUniqueId())).toItemStack());
                    }
                    player.openInventory(inventory);
                } else {
                    player.sendMessage("§dError: This player hasn't played before on the server.");
                }
            } else {
                Account account = Account.getAccount(player);
                Inventory inventory = Bukkit.createInventory(null, 2*9, "§d"+player.getName()+"§e's stats");
                inventory.addItem(new ItemBuilder(Material.SUGAR).setName("§a"+"Global").setLore("§eElo: §d"+ELOManager.getGlobalElo(player.getUniqueId())).toItemStack());
                for(Ladder ladder : Ladder.values()) {
                    inventory.addItem(new ItemBuilder(ladder.getItemStack()).setName("§a"+ladder.getName()).setLore("§eElo: §d"+ELOManager.getLadderElo(ladder, player.getUniqueId())).toItemStack());
                }
                player.openInventory(inventory);
            }
        }

        return false;
    }
}
