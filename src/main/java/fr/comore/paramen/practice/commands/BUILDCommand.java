package fr.comore.paramen.practice.commands;

import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.rank.accounts.Account;
import fr.comore.paramen.rank.ranks.RankUnit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BUILDCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            PracticePlayer practicePlayer = PracticePlayer.getAccount(player);

            if(strings.length == 1) {
                player.sendMessage("X: "+player.getLocation().getX());
                player.sendMessage("Y: "+player.getLocation().getY());
                player.sendMessage("Z: "+player.getLocation().getZ());
                player.sendMessage("YAW: "+player.getLocation().getYaw());
                player.sendMessage("PITCH: "+player.getLocation().getPitch());
            } else {
                Account account = Account.getAccount(player);
                if(account.getRank().getPower() >= RankUnit.ADMIN.getPower() || account.getRank() == RankUnit.BUILDER) {
                    practicePlayer.setBuildMode(!practicePlayer.isInBuildMode());
                    ParaPractice.scoreboard.get(player.getUniqueId()).setBuildModeOnLobbyScoreboard();
                    player.sendMessage(practicePlayer.isInBuildMode() ? "§aBuild Mode has been enabled." : "§cBuild mode has been disabled.");
                } else {
                    player.sendMessage("§dError: You don't have the necessary permissions.");
                }
            }
        }

        return false;
    }
}
