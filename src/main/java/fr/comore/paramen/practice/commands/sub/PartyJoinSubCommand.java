package fr.comore.paramen.practice.commands.sub;

import fr.comore.paramen.practice.party.PartyManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PartyJoinSubCommand extends PartySubCommand {

    String[] args;
    Player player;
    public PartyJoinSubCommand(String[] args, Player player) {
        this.args = args;
        this.player = player;
    }

    @Override
    public String command() {
        return "party";
    }

    @Override
    public String helpDescription() {
        return "Joins a party";
    }

    @Override
    public String[] args() {
        return args;
    }

    @Override
    public void execute() {
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("join")) {
                if(Bukkit.getPlayer(args[1]) == null) {
                    player.sendMessage("§eThis player is not online.");
                    return;
                }
                Player target = Bukkit.getPlayer(args[1]);
                PartyManager.joinParty(PracticePlayer.getAccount(player), PracticePlayer.getAccount(target));
            }
        }
    }
}
