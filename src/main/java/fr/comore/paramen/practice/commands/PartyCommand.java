package fr.comore.paramen.practice.commands;

import fr.comore.paramen.practice.commands.sub.PartyCreateSubCommand;
import fr.comore.paramen.practice.commands.sub.PartyDisbandSubCommand;
import fr.comore.paramen.practice.commands.sub.PartyJoinSubCommand;
import fr.comore.paramen.practice.commands.sub.PartyLeaveSubCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PartyCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if(strings.length >= 1) {
                new PartyCreateSubCommand(strings, player).execute();
                new PartyDisbandSubCommand(strings, player).execute();
                new PartyLeaveSubCommand(strings, player).execute();
                new PartyJoinSubCommand(strings, player).execute();
            } else {
                player.sendMessage("§e[==§dParty Commands§e==]");
                player.sendMessage("§d/party create §e- Creates a party");
                player.sendMessage("§d/party leave §e- Leaves a party");
                player.sendMessage("§d/party disband §e- Disbands a party");
                player.sendMessage("§d/party join §e- Joins a party");
            }
        }

        return false;
    }
}
