package fr.comore.paramen.practice.runnable;

import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.rank.ParaRank;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class MessageRunnable extends BukkitRunnable {

    public List<String> messages = new ArrayList<>();
    private int messageId = 0;

    public MessageRunnable() {
        init();
        this.runTaskTimer(ParaPractice.getInstance(), 0L, 12000L);
    }

    public void init() {
        messages.add("&6[PMN] §fIf you have a problem, please contact the staff");
        messages.add("&6[PMN] §fThink you're #1? Check out our leaderboards here: §ehttp://paramen.eu/leaderboards");
        messages.add("&6[PMN] §fApplications are now open! You can apply for staff here: §ehttp://forum.paramen.eu");
        messages.add("&6[PMN] §fEnjoy the server ? Help keep it up AND get some awesome perks by donating here: §ehttp://store.paramen.eu");
        messages.add("&6[PMN] §fEnjoy the server ? Help keep it up AND get some awesome perks by donating here: §ehttp://store.paramen.eu");
    }

    @Override
    public void run() {
        messageId++;
        if(messageId > messages.size() - 1) {
            messageId = 0;
        }

        Bukkit.broadcastMessage(messages.get(messageId).replace("&", "§"));
    }
}
