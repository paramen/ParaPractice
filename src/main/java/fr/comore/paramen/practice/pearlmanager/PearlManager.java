package fr.comore.paramen.practice.pearlmanager;

import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.practice.player.PracticePlayer;

import java.util.ArrayList;
import java.util.List;

public class PearlManager {

    private static List<PracticePlayer> pearlers = new ArrayList<>();

    public static void addPearler(PracticePlayer practicePlayer) {
        pearlers.add(practicePlayer);
        practicePlayer.getPlayer().sendMessage("§dYou threw an enderpearl, please wait 16 seconds to rethrew one.");
        ParaPractice.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(ParaPractice.getInstance(), () -> {
            if(practicePlayer.isInMatch()) {
                pearlers.remove(practicePlayer);
                practicePlayer.getPlayer().sendMessage("§dYou may use enderpearls.");
            }
        }, 20 * 16);
    }

    public static boolean isCooldowned(PracticePlayer practicePlayer) {
        return pearlers.contains(practicePlayer);
    }

    public static List<PracticePlayer> getPearlers() {
        return pearlers;
    }
}
