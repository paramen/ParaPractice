package fr.comore.paramen.practice.player;

import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.practice.SettingsManager;
import fr.comore.paramen.practice.elos.ELOManager;
import fr.comore.paramen.practice.kits.KitEditor;
import fr.comore.paramen.practice.kits.KitManager;
import fr.comore.paramen.practice.ladders.Ladder;
import fr.comore.paramen.practice.matchs.MatchManager;
import fr.comore.paramen.practice.party.PartyManager;
import fr.comore.paramen.practice.pearlmanager.PearlManager;
import fr.comore.paramen.practice.utils.ItemBuilder;
import fr.comore.paramen.rank.ParaRank;
import fr.comore.paramen.rank.accounts.Account;
import net.minecraft.server.v1_7_R4.ChatSerializer;
import net.minecraft.server.v1_7_R4.Packet;
import net.minecraft.server.v1_7_R4.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.sql.SQLException;

public class PracticePlayer {

    private Player player;

    public PracticePlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public Account getAccount() {
        return Account.getAccount(player);
    }

    public PracticePlayer setup() {
        ParaPractice.getInstance().getAccounts().add(this);
        boolean hasAccount = (boolean) ParaRank.getInstance().getMySQL().query("SELECT global_elo FROM elos WHERE player_uuid='"+player.getUniqueId().toString()+"'", resultSet -> {
            try {
                return resultSet.next();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return false;
        });
        if(!hasAccount) {
            ParaRank.getInstance().getMySQL().update("INSERT INTO elos (player_uuid) VALUES ('"+player.getUniqueId().toString()+"')");
        }
        KitEditor.setup(this);
        SettingsManager.setup(this);
        return this;
    }

    public PracticePlayer delete() {
        ParaPractice.getInstance().getAccounts().remove(this);
        return this;
    }

    public PracticePlayer openUnrankedInventory() {
        if(!PartyManager.isInParty(this)) {

            Inventory inventory = Bukkit.createInventory(null, 1*9, "§aUnranked queue");

            for(Ladder ladder : Ladder.values()) {
                inventory.addItem(new ItemBuilder(ladder.getItemStack()).setName("§6"+ladder.getName()).setLore("§ePlaying: §6"+ladder.getLadderType().unranked().size(), "§eQueued: §6"+ladder.getLadderType().queueUnranked().size()).toItemStack());
            }

            player.openInventory(inventory);
        }
        return this;
    }

    public PracticePlayer openRankedInventory() {
        if(!PartyManager.isInParty(this)) {
            Inventory inventory = Bukkit.createInventory(null, 1*9, "§eRanked queue");

            for(Ladder ladder : Ladder.values()) {
                inventory.addItem(new ItemBuilder(ladder.getItemStack()).setName("§6"+ladder.getName()).setLore("§ePlaying: §6"+ladder.getLadderType().ranked().size(), "§eQueued: §6"+ladder.getLadderType().queueRanked().size(), " ", "§eELO: §6"+ELOManager.getLadderElo(ladder, player.getUniqueId())).toItemStack());
            }

            player.openInventory(inventory);
        }
        return this;
    }

    public PracticePlayer openEditorInventory() {
        Inventory inventory = Bukkit.createInventory(null, 1*9, "§2Kit Editor");

        for(Ladder ladder : Ladder.values()) {
            if(ladder.isEditable()) {
                inventory.addItem(new ItemBuilder(ladder.getItemStack()).setName("§6"+ladder.getName()).setLore("").toItemStack());
            }
        }

        player.openInventory(inventory);

        return this;
    }

    public PracticePlayer sendJSONMessage(String message) {
        Packet packet = new PacketPlayOutChat(ChatSerializer.a(message), true);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
        return this;
    }

    public PracticePlayer openDuelInventory(PracticePlayer targetPracticePlayer) {

        if(!PartyManager.isInParty(this)) {

            Inventory inventory = Bukkit.createInventory(null, 1*9, "§9Duel " + targetPracticePlayer.getPlayer().getName());

            for(Ladder ladder : Ladder.values()) {
                inventory.addItem(new ItemBuilder(ladder.getItemStack()).setName("§6"+ladder.getName()).setLore("").toItemStack());
            }

            player.openInventory(inventory);
        }

        return this;
    }

    public PracticePlayer setLobbyItems() {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setArmorContents(null);
        player.getActivePotionEffects().forEach(potionEffect -> player.removePotionEffect(potionEffect.getType()));
        player.teleport(Bukkit.getWorld("world").getSpawnLocation());
        player.setHealth(20);
        player.setFoodLevel(20);
        player.setFlying(false);
        player.setAllowFlight(false);
        player.setGameMode(GameMode.SURVIVAL);
        if(PartyManager.isInParty(this)) {
            setPartyInventory();
            return this;
        }
        playerInventory.setItem(0, new ItemBuilder(Material.STONE_SWORD).setName("§aUnranked queue").setUnbreakable(true).toItemStack());
        playerInventory.setItem(1, new ItemBuilder(Material.IRON_SWORD).setName("§eRanked queue").setUnbreakable(true).toItemStack());
        playerInventory.setItem(4, new ItemBuilder(Material.NAME_TAG).setName("§9Create party").setUnbreakable(true).toItemStack());
        playerInventory.setItem(6, new ItemBuilder(Material.EMERALD).setName("§bView Leaderboards").setUnbreakable(true).toItemStack());
        playerInventory.setItem(7, new ItemBuilder(Material.WATCH).setName("§3Settings").setUnbreakable(true).toItemStack());
        playerInventory.setItem(8, new ItemBuilder(Material.BOOK).setName("§2Kit Editor").setUnbreakable(true).toItemStack());
        player.updateInventory();
        ParaPractice.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(ParaPractice.getInstance(), () -> {

            player.setHealth(20D);
            if(PearlManager.isCooldowned(this)) {
                PearlManager.getPearlers().remove(this);
            }

        }, 20 * 2);
        return this;
    }

    public PracticePlayer setLobbyItems(String lol) {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setArmorContents(null);
        player.getActivePotionEffects().forEach(potionEffect -> player.removePotionEffect(potionEffect.getType()));
        player.teleport(Bukkit.getWorld("world").getSpawnLocation());
        player.setHealth(20);
        player.setFoodLevel(20);
        player.setFlying(false);
        player.setAllowFlight(false);
        player.setGameMode(GameMode.SURVIVAL);
        playerInventory.setItem(0, new ItemBuilder(Material.STONE_SWORD).setName("§aUnranked queue").setUnbreakable(true).toItemStack());
        playerInventory.setItem(1, new ItemBuilder(Material.IRON_SWORD).setName("§eRanked queue").setUnbreakable(true).toItemStack());
        playerInventory.setItem(4, new ItemBuilder(Material.NAME_TAG).setName("§9Create party").setUnbreakable(true).toItemStack());
        playerInventory.setItem(6, new ItemBuilder(Material.EMERALD).setName("§bView Leaderboards").setUnbreakable(true).toItemStack());
        playerInventory.setItem(7, new ItemBuilder(Material.WATCH).setName("§3Settings").setUnbreakable(true).toItemStack());
        playerInventory.setItem(8, new ItemBuilder(Material.BOOK).setName("§2Kit Editor").setUnbreakable(true).toItemStack());
        player.updateInventory();
        ParaPractice.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(ParaPractice.getInstance(), () -> {

            player.setHealth(20D);
            if(PearlManager.isCooldowned(this)) {
                PearlManager.getPearlers().remove(this);
            }

        }, 20 * 2);
        return this;
    }

    public PracticePlayer setQueueItems() {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setItem(0, new ItemBuilder(Material.REDSTONE).setName("§7Leave queue").setUnbreakable(true).toItemStack());
        return this;
    }

    public PracticePlayer setSpectatorItems() {
        if(!PartyManager.isInParty(this)) {

            PlayerInventory playerInventory = player.getInventory();
            playerInventory.clear();
            playerInventory.setItem(0, new ItemBuilder(Material.REDSTONE).setName("§7Stop spectate").setUnbreakable(true).toItemStack());

        }
        return this;
    }

    public static PracticePlayer getAccount(Player player) {
        return ParaPractice.getInstance().getAccounts().stream().filter(practicePlayer -> practicePlayer.getPlayer() == player).findFirst().get();
    }

    public PracticePlayer hideAllPlayers(PracticePlayer practicePlayer) {
        for(Player playerT : Bukkit.getServer().getOnlinePlayers()) {
            if(playerT != practicePlayer.getPlayer()) player.hidePlayer(playerT);
        }
        return this;
    }


    public PracticePlayer hideAllPlayers(PracticePlayer practicePlayer, boolean grreg) {
        for(Player playerT : Bukkit.getServer().getOnlinePlayers()) {
            if(playerT != practicePlayer.getPlayer()) player.hidePlayer(playerT);
        }
        player.showPlayer(practicePlayer.getPlayer());
        player.showPlayer(MatchManager.getEnemy(practicePlayer).getPlayer());
        return this;
    }
    public PracticePlayer showAllPlayers() {
        for(Player playerT : Bukkit.getServer().getOnlinePlayers()) {
            player.showPlayer(playerT);
        }
        return this;
    }

    public boolean isInMatch() {
        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().unranked().containsKey(this)) return true;
            if(ladder.getLadderType().ranked().containsKey(this)) return true;
        }
        return false;
    }

    public boolean isInQueue() {
        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().queueUnranked().contains(this)) return true;
            if(ladder.getLadderType().queueRanked().contains(this)) return true;
        }
        return false;
    }

    public PracticePlayer setBuildMode(boolean buildMode) {
        if(buildMode) {
            ParaPractice.builders.add(player);
        } else {
            ParaPractice.builders.remove(player);
        }
        return this;
    }

    public boolean isInBuildMode() {
        return ParaPractice.builders.contains(player);
    }

    public PracticePlayer openSettingsInventory() {
        if(!isInMatch() && !isInQueue()) {
            Inventory inventory = Bukkit.createInventory(null, 1*9, "§3Settings");
            for(SettingsManager.SettingsList settingsList : SettingsManager.SettingsList.values()) {
                if(SettingsManager.getSetting(this, settingsList)) {
                    inventory.addItem(settingsList.getEnabledItemStack());
                } else {
                    inventory.addItem(settingsList.getDisabledItemStack());
                }
            }
            player.openInventory(inventory);
        } else {
            player.sendMessage("§dYou can't do this in this status.");
        }
        return this;
    }

    public PracticePlayer openLeaderboardInventory() {
        Inventory inventory = Bukkit.createInventory(null, 3*9, "§eLeaderboards");
        inventory.setItem(0, new ItemStack(Material.STONE));
        inventory.setItem(8, new ItemStack(Material.STONE));
        inventory.setItem(9, new ItemStack(Material.STONE));
        inventory.setItem(17, new ItemStack(Material.STONE));
        inventory.setItem(18, new ItemStack(Material.STONE));
        inventory.setItem(26, new ItemStack(Material.STONE));
        inventory.addItem(new ItemBuilder(Material.SUGAR).setName("§d"+ "Global" + " §e| Top 10").setLore("§e--------------------", "§e#1: §d-§e (?)", "§e#2: §d-§e (?)", "§e#3: §d-§e (?)", "§e#4: §d-§e (?)", "§e#5: §d-§e (?)", "§e#6: §d-§e (?)", "§e#7: §d-§e (?)", "§e#8: §d-§e (?)", "§e#9: §d-§e (?)", "§e#10: §d-§e (?)", "§e--------------------").toItemStack());

        for(Ladder ladder : Ladder.values()) {
            inventory.addItem(new ItemBuilder(ladder.getItemStack()).setName("§d"+ladder.getName() + " §e| Top 10").setLore("§e--------------------", "§e#1: §d-§e (?)", "§e#2: §d-§e (?)", "§e#3: §d-§e (?)", "§e#4: §d-§e (?)", "§e#5: §d-§e (?)", "§e#6: §d-§e (?)", "§e#7: §d-§e (?)", "§e#8: §d-§e (?)", "§e#9: §d-§e (?)", "§e#10: §d-§e (?)", "§e--------------------").toItemStack());
        }

        inventory.setItem(0, new ItemStack(Material.AIR));
        inventory.setItem(8, new ItemStack(Material.AIR));
        inventory.setItem(9, new ItemStack(Material.AIR));
        inventory.setItem(17, new ItemStack(Material.AIR));
        inventory.setItem(18, new ItemStack(Material.AIR));
        inventory.setItem(26, new ItemStack(Material.AIR));

        player.openInventory(inventory);
        return this;
    }

    public PracticePlayer setPartyInventory() {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setItem(0, new ItemBuilder(Material.STONE_SWORD).setName("§aJoin 2v2 Unranked queue").toItemStack());
        playerInventory.setItem(1, new ItemBuilder(Material.IRON_SWORD).setName("§eJoin 2v2 Ranked queue").toItemStack());
        playerInventory.setItem(4, new ItemBuilder(Material.IRON_AXE).setName("§bStart party event").toItemStack());
        playerInventory.setItem(5, new ItemBuilder(Material.DIAMOND_AXE).setName("§bFight other party").toItemStack());
        playerInventory.setItem(7, new ItemBuilder(Material.SKULL_ITEM).setName("§9View Party Members").toItemStack());
        playerInventory.setItem(8, new ItemBuilder(Material.NETHER_STAR).setName("§7Leave party").toItemStack());
        player.updateInventory();
        return this;
    }

    public PracticePlayer openELOInventory(PracticePlayer practicePlayer) {
        if(practicePlayer.getPlayer() == null) return this;
        Inventory inventory = Bukkit.createInventory(null, 2*9, "§d"+practicePlayer.getPlayer().getName()+"§e's stats");
        inventory.addItem(new ItemBuilder(Material.SUGAR).setName("§a"+"Global").setLore("§eElo: §d"+ELOManager.getGlobalElo(player.getUniqueId())).toItemStack());
        for(Ladder ladder : Ladder.values()) {
            inventory.addItem(new ItemBuilder(ladder.getItemStack()).setName("§a"+ladder.getName()).setLore("§eElo: §d"+ELOManager.getLadderElo(ladder, practicePlayer.getPlayer().getUniqueId())).toItemStack());
        }
        player.openInventory(inventory);
        return this;
    }

    public PracticePlayer openELOInventory() {

        Inventory inventory = Bukkit.createInventory(null, 2*9, "§d"+this.getPlayer().getName()+"§e's stats");
        inventory.addItem(new ItemBuilder(Material.SUGAR).setName("§a"+"Global").setLore("§eElo: §d"+ELOManager.getGlobalElo(player.getUniqueId())).toItemStack());
        for(Ladder ladder : Ladder.values()) {
            inventory.addItem(new ItemBuilder(ladder.getItemStack()).setName("§a"+ladder.getName()).setLore("§eElo: §d"+ELOManager.getLadderElo(ladder, player.getUniqueId())).toItemStack());
        }
        player.openInventory(inventory);
        return this;
    }

    @Override
    public String toString() {
        return player.getName();
    }
}
