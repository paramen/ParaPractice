package fr.comore.paramen.practice;

import fr.comore.paramen.practice.commands.*;
import fr.comore.paramen.practice.listeners.*;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.runnable.MessageRunnable;
import fr.comore.paramen.practice.scoreboard.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public class ParaPractice extends JavaPlugin {

    private static ParaPractice instance;

    private List<PracticePlayer> practicePlayers;

    public static HashMap<UUID, ScoreboardManager> scoreboard = new HashMap<UUID, ScoreboardManager>();
    public static List<PracticePlayer> noDebuffUnrankedQueue;
    public static List<PracticePlayer> debuffUnrankedQueue;
    public static List<PracticePlayer> sumoUnrankedQueue;
    public static List<PracticePlayer> noDebuffRankedQueue;
    public static List<PracticePlayer> debuffRankedQueue;
    public static List<PracticePlayer> sumoRankedQueue;
    public static List<PracticePlayer> axeUnrankedQueue;
    public static List<PracticePlayer> axeRankedQueue;
    public static List<PracticePlayer> archerUnrankedQueue;
    public static List<PracticePlayer> archerRankedQueue;
    public static List<Player> builders;

    @Override
    public void onEnable() {
        instance = this;
        registerQueues();
        builders = new ArrayList<>();
        registerComponents();
        super.onEnable();
    }

    private void registerQueues() {
        practicePlayers = new ArrayList<>();
        noDebuffUnrankedQueue = new ArrayList<>();
        debuffUnrankedQueue = new ArrayList<>();
        noDebuffRankedQueue = new ArrayList<>();
        debuffRankedQueue = new ArrayList<>();
        sumoUnrankedQueue = new ArrayList<>();
        sumoRankedQueue = new ArrayList<>();
        axeRankedQueue = new ArrayList<>();
        axeUnrankedQueue = new ArrayList<>();
        archerUnrankedQueue = new ArrayList<>();
        archerRankedQueue = new ArrayList<>();
    }

    @Override
    public void onDisable() {
        for(Player player : Bukkit.getServer().getOnlinePlayers()) { player.kickPlayer("§eSorry, but the server is reloading, please wait a minute."); }
        super.onDisable();
    }

    private void registerComponents() {
        new MessageRunnable();
        PluginManager pluginManager = Bukkit.getServer().getPluginManager();
        pluginManager.registerEvents(new JoinListener(), this);
        pluginManager.registerEvents(new InteractListener(), this);
        pluginManager.registerEvents(new KillEvent(), this);
        pluginManager.registerEvents(new WeatherStopChange(), this);
        pluginManager.registerEvents(new PearlEvent(this), this);
        pluginManager.registerEvents(new BuildEvent(), this);
        getCommand("elo").setExecutor(new ELOCommand());
        getCommand("viewelo").setExecutor(new ELOCommand());
        getCommand("build").setExecutor(new BUILDCommand());
        getCommand("duel").setExecutor(new DUELCommand());
        getCommand("accept").setExecutor(new ACCEPTCommand());
        getCommand("rank").setExecutor(new RankCommand());
        getCommand("setrank").setExecutor(new RankCommand());
        getCommand("gm").setExecutor(new GameModeCommand());
        getCommand("gamemode").setExecutor(new GameModeCommand());
        getCommand("spec").setExecutor(new SPECCommand());
        getCommand("spectate").setExecutor(new SPECCommand());
        getCommand("inventory").setExecutor(new INVCommand());
        getCommand("inv").setExecutor(new INVCommand());
        getCommand("settings").setExecutor(new SETTINGSCommand());
        getCommand("options").setExecutor(new SETTINGSCommand());
        getCommand("ping").setExecutor(new PINGCommand());
        getCommand("staff").setExecutor(new STAFFCommand());
        getCommand("staffchat").setExecutor(new STAFFCommand());
        getCommand("sc").setExecutor(new STAFFCommand());
        getCommand("report").setExecutor(new REPORTCommand());
        getCommand("party").setExecutor(new PartyCommand());
    }

    public static ParaPractice getInstance() {
        return instance;
    }

    public List<PracticePlayer> getAccounts() {
        return practicePlayers;
    }
}
