package fr.comore.paramen.practice.scoreboard;
import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.practice.SettingsManager;
import fr.comore.paramen.practice.elos.ELOManager;
import fr.comore.paramen.practice.kits.KitManager;
import fr.comore.paramen.practice.matchs.MatchManager;
import fr.comore.paramen.practice.party.PartyManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.queue.QueueManager;
import fr.comore.paramen.rank.accounts.Account;
import fr.comore.paramen.rank.ranks.RankUnit;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.Random;
import java.util.Set;


public class ScoreboardManager implements org.bukkit.scoreboard.ScoreboardManager {

    private Scoreboard sb;
    private Objective right;
    private String name = "right";
    private Player player;
    private ScoreboardHelper dynamicScoreboard;

    public ScoreboardManager(Player player) {
        sb = Bukkit.getScoreboardManager().getNewScoreboard();
        dynamicScoreboard = new ScoreboardHelper(sb, "§d§lPRACTICE");
        this.player = player;
    }

    public void setTag() {
        for(ScoreboardManager sbList : ParaPractice.scoreboard.values()){
            for(Player all : Bukkit.getServer().getOnlinePlayers()){
                Team team = sbList.sb.getTeam(""+Account.getAccount(all).getRank().getScoreboardPower());
                team.addPlayer(all);
            }
        }
    }

    public void setMatchScoreboard() {
        if(SettingsManager.getSetting(PracticePlayer.getAccount(player), SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {
            dynamicScoreboard.clear();
            dynamicScoreboard.add("§7§m------------------");
            dynamicScoreboard.add("§fOpponent: §d"+MatchManager.getEnemy(PracticePlayer.getAccount(player)).getPlayer().getName());
            dynamicScoreboard.add(" ");
            dynamicScoreboard.add("§fYour rank: ");
            dynamicScoreboard.add(Account.getAccount(player).getRank().getNameColor());
            dynamicScoreboard.add(" ");
            if(MatchManager.isRanked(PracticePlayer.getAccount(player))) {
                dynamicScoreboard.add("§fELO: §d"+ELOManager.getLadderElo(MatchManager.getMatchLadder(PracticePlayer.getAccount(player)), player.getUniqueId()));
                dynamicScoreboard.add(" ");
            }
            dynamicScoreboard.add("§dparamen.eu");
            dynamicScoreboard.add("§7§m------------------");
            dynamicScoreboard.update(player);
        }

    }
    public void setQueueScoreboard() {
        if(SettingsManager.getSetting(PracticePlayer.getAccount(player), SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {
            dynamicScoreboard.clear();
            int online = 0;
            for(Player players : Bukkit.getServer().getOnlinePlayers()) {
                online++;
            }
            dynamicScoreboard.add("§7§m------------------");
            dynamicScoreboard.add("§fOnline: §d"+online);
            dynamicScoreboard.add("§fQueued: §d"+(new QueueManager().getLadder(PracticePlayer.getAccount(player)).getLadderType().queueUnranked().size() + new QueueManager().getLadder(PracticePlayer.getAccount(player)).getLadderType().queueRanked().size()));
            dynamicScoreboard.add(" ");
            dynamicScoreboard.add("§fQueue:");
            dynamicScoreboard.add("§d"+new QueueManager().getType(PracticePlayer.getAccount(player)) + " " + new QueueManager().getLadder(PracticePlayer.getAccount(player)).getName());
            dynamicScoreboard.add(" ");
            dynamicScoreboard.add("§dparamen.eu");
            dynamicScoreboard.add("§7§m------------------");
            dynamicScoreboard.update(player);
        }
    }
    public void setQueueScoreboard(boolean disconnect) {
        if(SettingsManager.getSetting(PracticePlayer.getAccount(player), SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {

            dynamicScoreboard.clear();
            int online = 0;
            for(Player players : Bukkit.getServer().getOnlinePlayers()) {
                online++;
            }
            dynamicScoreboard.add("§7§m------------------");
            dynamicScoreboard.add("§fOnline: §d"+(online - 1));
            dynamicScoreboard.add("§fQueued: §d"+(new QueueManager().getLadder(PracticePlayer.getAccount(player)).getLadderType().queueUnranked().size() + new QueueManager().getLadder(PracticePlayer.getAccount(player)).getLadderType().queueRanked().size()));
            dynamicScoreboard.add(" ");
            dynamicScoreboard.add("§fQueue:");
            dynamicScoreboard.add("§d"+new QueueManager().getType(PracticePlayer.getAccount(player)) + " " + new QueueManager().getLadder(PracticePlayer.getAccount(player)).getName());
            dynamicScoreboard.add(" ");
            dynamicScoreboard.add("§dparamen.eu");
            dynamicScoreboard.add("§7§m------------------");
            dynamicScoreboard.update(player);
        }
    }

    public void setLobbyScoreboard() {
        if(SettingsManager.getSetting(PracticePlayer.getAccount(player), SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {

            dynamicScoreboard.clear();
            int online = 0;
            int fighting = 0;
            for(Player players : Bukkit.getServer().getOnlinePlayers()) {
                online++;
                if(PracticePlayer.getAccount(players).isInMatch()) fighting++;
            }
            dynamicScoreboard.add("§7§m------------------");
            dynamicScoreboard.add("§fOnline: §d"+online);
            dynamicScoreboard.add("§fPlaying: §d"+fighting);
            dynamicScoreboard.add(" ");
            if(PartyManager.isInParty(PracticePlayer.getAccount(player))) {
                dynamicScoreboard.add("§fParty:");
                dynamicScoreboard.add("§fLeader: §d"+PartyManager.getPartyLeader(PracticePlayer.getAccount(player)).getPlayer().getName());
                dynamicScoreboard.add("§fMembers: §d"+(PartyManager.getMembers(PracticePlayer.getAccount(player)).size() + 1));
                dynamicScoreboard.add("");
            }
            dynamicScoreboard.add("§dparamen.eu");
            dynamicScoreboard.add("§7§m------------------");
            setTag();
            dynamicScoreboard.update(player);
        }
    }

    public void setLobbyScoreboard(String lol) {
        if(SettingsManager.getSetting(PracticePlayer.getAccount(player), SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {

            dynamicScoreboard.clear();
            int online = 0;
            int fighting = 0;
            for(Player players : Bukkit.getServer().getOnlinePlayers()) {
                online++;
                if(PracticePlayer.getAccount(players).isInMatch()) fighting++;
            }
            dynamicScoreboard.add("§7§m------------------");
            dynamicScoreboard.add("§fOnline: §d"+online);
            dynamicScoreboard.add("§fPlaying: §d"+fighting);
            dynamicScoreboard.add(" ");
            dynamicScoreboard.add("§dparamen.eu");
            dynamicScoreboard.add("§7§m------------------");
            setTag();
            dynamicScoreboard.update(player);
        }
    }

    public void setLobbyScoreboard(boolean disconnect) {
        if(SettingsManager.getSetting(PracticePlayer.getAccount(player), SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {

            dynamicScoreboard.clear();
            int online = 0;
            int fighting = 0;
            for(Player players : Bukkit.getServer().getOnlinePlayers()) {
                online++;
                if(PracticePlayer.getAccount(players).isInMatch()) fighting++;
            }
            dynamicScoreboard.add("§7§m------------------");
            dynamicScoreboard.add("§fOnline: §d"+ (online - 1));
            dynamicScoreboard.add("§fPlaying: §d"+fighting);
            dynamicScoreboard.add(" ");
            if(PartyManager.isInParty(PracticePlayer.getAccount(player))) {
                dynamicScoreboard.add("§fParty:");
                dynamicScoreboard.add("§fLeader: §d"+PartyManager.getPartyLeader(PracticePlayer.getAccount(player)).getPlayer().getName());
                dynamicScoreboard.add("§fMembers: §d"+(PartyManager.getMembers(PracticePlayer.getAccount(player)).size() + 1));
                dynamicScoreboard.add("");
            }
            dynamicScoreboard.add("§dparamen.eu");
            dynamicScoreboard.add("§7§m------------------");
            dynamicScoreboard.update(player);
        }
    }

    public void clearScoreboard() {
        dynamicScoreboard.clear();
        dynamicScoreboard.update(player);
    }

    public void setBuildModeOnLobbyScoreboard() {
        if(!PracticePlayer.getAccount(player).isInMatch() && !PracticePlayer.getAccount(player).isInQueue()) {

            if(SettingsManager.getSetting(PracticePlayer.getAccount(player), SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {
                setLobbyScoreboard();
            }
        }
    }

    public void refresh(){
        dynamicScoreboard.clear();
        if(PracticePlayer.getAccount(player).isInMatch()) {
            setMatchScoreboard();
        } if(PracticePlayer.getAccount(player).isInQueue()) {
            setQueueScoreboard();
        } else {
            setLobbyScoreboard();
        }
        dynamicScoreboard.update(player);
    }

    @SuppressWarnings("deprecation")
    public void init(){
        player.setScoreboard(sb);
        ParaPractice.scoreboard.put(player.getUniqueId(), this);

        for(RankUnit rank : RankUnit.values()){
            if(sb.getTeam(""+rank.getScoreboardPower()) == null){
                Team team = sb.registerNewTeam(""+rank.getScoreboardPower());
            }
            sb.getTeam(""+rank.getScoreboardPower()).setPrefix(rank.getScoreboardPrefix());
        }

        setTag();

        if(SettingsManager.getSetting(PracticePlayer.getAccount(player), SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {
            refresh();
        }

    }

    public Objective getRight(){
        return right;
    }

    @Override
    public Scoreboard getMainScoreboard() {
        return sb;
    }

    @Override
    public Scoreboard getNewScoreboard() {
        return Bukkit.getScoreboardManager().getNewScoreboard();
    }

    public ScoreboardHelper getDynamicScoreboard() {
        return dynamicScoreboard;
    }
}

