package fr.comore.paramen.practice.scoreboard;

import fr.comore.paramen.rank.ranks.RankUnit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class NameTagChanger {
    private static Team team;
    private static Scoreboard scoreboard;

    public static void changePlayerName(Player player, RankUnit rankUnit) {
        if (player.getScoreboard() == null || rankUnit == null) {
            return;
        }

        scoreboard = player.getScoreboard();
        if (scoreboard.getTeam(""+rankUnit.getScoreboardPower()) == null) {
            scoreboard.registerNewTeam(""+rankUnit.getScoreboardPower());
        }
        team = scoreboard.getTeam(""+rankUnit.getScoreboardPower());
        team.setPrefix(rankUnit.getPrefix());
        team.addPlayer(player);
    }

    private static String Color(String input) {
        return ChatColor.translateAlternateColorCodes('&', input);
    }

}
