package fr.comore.paramen.practice.queue;

import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.practice.SettingsManager;
import fr.comore.paramen.practice.ladders.Ladder;
import fr.comore.paramen.practice.ladders.LadderType;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.scoreboard.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class QueueManager {


    public void addUnrankedQueue(PracticePlayer practicePlayer, Ladder ladder) {
        LadderType ladderType = ladder.getLadderType();
        ladderType.queueUnranked().add(practicePlayer);
        ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).refresh();
        for(Map.Entry<UUID, ScoreboardManager> entry : ParaPractice.scoreboard.entrySet()) {
            Player players = Bukkit.getPlayer(entry.getKey());
            PracticePlayer practicePlayers = PracticePlayer.getAccount(players);

            if(practicePlayers.isInQueue()) {
                entry.getValue().setQueueScoreboard();
            }
        }
    }

    public void removeUnrankedQueue(PracticePlayer practicePlayer) {
        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().queueUnranked().contains(practicePlayer)) {
                ladder.getLadderType().queueUnranked().remove(practicePlayer);
                ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).refresh();
            }
        }

        for(Map.Entry<UUID, ScoreboardManager> entry : ParaPractice.scoreboard.entrySet()) {
            Player players = Bukkit.getPlayer(entry.getKey());
            PracticePlayer practicePlayers = PracticePlayer.getAccount(players);

            if(practicePlayers.isInQueue()) {
                if(SettingsManager.getSetting(practicePlayers, SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {
                    entry.getValue().setQueueScoreboard();
                }
            }
        }
    }
    public void addRankedQueue(PracticePlayer practicePlayer, Ladder ladder) {
        LadderType ladderType = ladder.getLadderType();
        ladderType.queueRanked().add(practicePlayer);
        ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).refresh();
        for(Map.Entry<UUID, ScoreboardManager> entry : ParaPractice.scoreboard.entrySet()) {
            Player players = Bukkit.getPlayer(entry.getKey());
            PracticePlayer practicePlayers = PracticePlayer.getAccount(players);

            if(practicePlayers.isInQueue()) {
                if(SettingsManager.getSetting(practicePlayers, SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {
                    entry.getValue().setQueueScoreboard();
                }
            }
        }
    }

    public void removeRankedQueue(PracticePlayer practicePlayer) {
        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().queueRanked().contains(practicePlayer)) {
                ladder.getLadderType().queueRanked().remove(practicePlayer);
                ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).refresh();
            }
        }

        for(Map.Entry<UUID, ScoreboardManager> entry : ParaPractice.scoreboard.entrySet()) {
            Player players = Bukkit.getPlayer(entry.getKey());
            PracticePlayer practicePlayers = PracticePlayer.getAccount(players);

            if(practicePlayers.isInQueue()) {
                entry.getValue().setQueueScoreboard();
            }
        }
    }
    public boolean canRankedMatch(Ladder ladder) {
        LadderType ladderType = ladder.getLadderType();

        return ladderType.queueRanked().size() >= 1;
    }
    public boolean canUnrankedMatch(Ladder ladder) {
        LadderType ladderType = ladder.getLadderType();

        return ladderType.queueUnranked().size() >= 1;
    }

    public Ladder getLadder(PracticePlayer practicePlayer) {
        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().queueUnranked().contains(practicePlayer)) return ladder;
            if(ladder.getLadderType().queueRanked().contains(practicePlayer)) return ladder;
        }
        return null;
    }

    public String getType(PracticePlayer practicePlayer) {
        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().queueUnranked().contains(practicePlayer)) return "Unranked";
            if(ladder.getLadderType().queueRanked().contains(practicePlayer)) return "Ranked";
        }
        return "Doesn't know";
    }

    public List<PracticePlayer> getDebuffUnrankedQueue() {
        return ParaPractice.debuffUnrankedQueue;
    }

    public List<PracticePlayer> getNoDebuffUnrankedQueue() {
        return ParaPractice.noDebuffUnrankedQueue;
    }

    public List<PracticePlayer> getNoDebuffRankedQueue() {
        return ParaPractice.noDebuffRankedQueue;
    }

    public List<PracticePlayer> getDebuffRankedQueue() {
        return ParaPractice.debuffRankedQueue;
    }

    public List<PracticePlayer> getSumoRankedQueue() {
        return ParaPractice.sumoRankedQueue;
    }

    public List<PracticePlayer> getSumoUnrankedQueue() {
        return ParaPractice.sumoUnrankedQueue;
    }

    public List<PracticePlayer> getAxeRankedQueue() {
        return ParaPractice.axeRankedQueue;
    }

    public List<PracticePlayer> getAxeUnrankedQueue() {
        return ParaPractice.axeUnrankedQueue;
    }


    public List<PracticePlayer> getArcherUnrankedQueue() { return ParaPractice.archerUnrankedQueue; }

    public List<PracticePlayer> getArcherRankedQueue() { return ParaPractice.archerRankedQueue; }

}
