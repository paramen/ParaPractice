package fr.comore.paramen.practice.matchs;

import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.practice.SettingsManager;
import fr.comore.paramen.practice.arena.Arena;
import fr.comore.paramen.practice.elos.ELOManager;
import fr.comore.paramen.practice.inventory.InventoryManager;
import fr.comore.paramen.practice.ladders.Ladder;
import fr.comore.paramen.practice.ladders.LadderType;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.scoreboard.ScoreboardManager;
import fr.comore.paramen.practice.spectator.SpectatorManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MatchManager {
    private static Map<PracticePlayer, PracticePlayer> noDebuffUnrankedMatches = new HashMap<>();
    private static Map<PracticePlayer, PracticePlayer> debuffUnrankedMatches = new HashMap<>();
    private static Map<PracticePlayer, PracticePlayer> sumoUnrankedMatches = new HashMap<>();
    private static Map<PracticePlayer, PracticePlayer> noDebuffRankedMatches = new HashMap<>();
    private static Map<PracticePlayer, PracticePlayer> debuffRankedMatches = new HashMap<>();
    private static Map<PracticePlayer, PracticePlayer> sumoRankedMatches = new HashMap<>();
    private static Map<PracticePlayer, PracticePlayer> axeUnrankedMatches = new HashMap<>();
    private static Map<PracticePlayer, PracticePlayer> axeRankedMatches = new HashMap<>();
    private static Map<PracticePlayer, PracticePlayer> archerUnrankedMatches = new HashMap<>();
    private static Map<PracticePlayer, PracticePlayer> archerRankedMatches = new HashMap<>();
    private static LadderType ladderType;

    public static void addUnrankedMatch(Ladder ladder, PracticePlayer practicePlayer1, PracticePlayer practicePlayer2) {

        ladderType = ladder.getLadderType();
        ladder.getLadderType().unranked().put(practicePlayer1, practicePlayer2);
        ladder.getLadderType().unranked().put(practicePlayer2, practicePlayer1);
        practicePlayer1.hideAllPlayers(practicePlayer2);
        practicePlayer2.hideAllPlayers(practicePlayer1);
        ladderType.give(practicePlayer1.getPlayer());
        ladderType.give(practicePlayer2.getPlayer());
        practicePlayer1.getPlayer().teleport(ladder.getArena().getLocation1());
        practicePlayer2.getPlayer().teleport(ladder.getArena().getLocation2());
        practicePlayer1.getPlayer().setGameMode(GameMode.SURVIVAL);
        practicePlayer2.getPlayer().setGameMode(GameMode.SURVIVAL);
        if(ladderType.queueUnranked().contains(practicePlayer1)) {
            ladderType.queueUnranked().remove(practicePlayer1);
        }
        if(ladderType.queueUnranked().contains(practicePlayer2)) {
            ladderType.queueUnranked().remove(practicePlayer2);
        }
        practicePlayer1.getPlayer().sendMessage("§eStarting a match with kit §d" + ladder.getName() + " §ebetween §d" + practicePlayer1.getPlayer().getName() + " §eand §d" + practicePlayer2.getPlayer().getName() + "§e.");
        practicePlayer2.getPlayer().sendMessage("§eStarting a match with kit §d" + ladder.getName() + " §ebetween §d" + practicePlayer2.getPlayer().getName() + " §eand §d" + practicePlayer1.getPlayer().getName() + "§e.");

        ParaPractice.scoreboard.get(practicePlayer1.getPlayer().getUniqueId()).setMatchScoreboard();
        ParaPractice.scoreboard.get(practicePlayer2.getPlayer().getUniqueId()).setMatchScoreboard();

        for(Map.Entry<UUID, ScoreboardManager> entry : ParaPractice.scoreboard.entrySet()) {
            Player players = Bukkit.getPlayer(entry.getKey());
            PracticePlayer practicePlayers = PracticePlayer.getAccount(players);

            if(practicePlayers.isInQueue()) {
                if(SettingsManager.getSetting(practicePlayers, SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {
                    entry.getValue().setQueueScoreboard();
                }
            } else if(!practicePlayers.isInMatch()) {
                if(SettingsManager.getSetting(practicePlayers, SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {
                    entry.getValue().setLobbyScoreboard();
                }
            }
        }
    }
    public static void addRankedMatch(Ladder ladder, PracticePlayer practicePlayer1, PracticePlayer practicePlayer2) {
        ladderType = ladder.getLadderType();
        ladderType.ranked().put(practicePlayer2, practicePlayer1);
        ladderType.ranked().put(practicePlayer1, practicePlayer2);
        practicePlayer1.hideAllPlayers(practicePlayer2);
        practicePlayer2.hideAllPlayers(practicePlayer1);
        ladderType.give(practicePlayer1.getPlayer());
        ladderType.give(practicePlayer2.getPlayer());
        practicePlayer1.getPlayer().teleport(ladder.getArena().getLocation1());
        practicePlayer2.getPlayer().teleport(ladder.getArena().getLocation2());
        practicePlayer1.getPlayer().setGameMode(GameMode.SURVIVAL);
        practicePlayer2.getPlayer().setGameMode(GameMode.SURVIVAL);
        if(ladderType.queueRanked().contains(practicePlayer1)) {
            ladderType.queueRanked().remove(practicePlayer1);
        }
        if(ladderType.queueRanked().contains(practicePlayer2)) {
            ladderType.queueRanked().remove(practicePlayer2);
        }
        ParaPractice.scoreboard.get(practicePlayer1.getPlayer().getUniqueId()).setMatchScoreboard();
        ParaPractice.scoreboard.get(practicePlayer2.getPlayer().getUniqueId()).setMatchScoreboard();

        practicePlayer1.getPlayer().sendMessage("§eStarting a match with kit §d" + ladder.getName() + " §ebetween §d" + practicePlayer1.getPlayer().getName() + " §eand §d" + practicePlayer2.getPlayer().getName() + "§e.");
        practicePlayer2.getPlayer().sendMessage("§eStarting a match with kit §d" + ladder.getName() + " §ebetween §d" + practicePlayer2.getPlayer().getName() + " §eand §d" + practicePlayer1.getPlayer().getName() + "§e.");

        for(Map.Entry<UUID, ScoreboardManager> entry : ParaPractice.scoreboard.entrySet()) {
            Player players = Bukkit.getPlayer(entry.getKey());
            PracticePlayer practicePlayers = PracticePlayer.getAccount(players);

            if(practicePlayers.isInQueue()) {
                if(SettingsManager.getSetting(PracticePlayer.getAccount(Bukkit.getPlayer(entry.getKey())), SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {
                    entry.getValue().setQueueScoreboard();
                }
            } else if(!practicePlayers.isInMatch()) {
                if(SettingsManager.getSetting(PracticePlayer.getAccount(Bukkit.getPlayer(entry.getKey())), SettingsManager.SettingsList.TOGGLE_SCOREBOARD)) {
                    entry.getValue().setLobbyScoreboard();
                }
            }
        }
    }

    public static void removeUnrankedMatch(PracticePlayer practicePlayer) {


        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().unranked().containsKey(practicePlayer)) {
                ladder.getLadderType().unranked().remove(practicePlayer);
                ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).refresh();
            }
        }

        for(Map.Entry<UUID, ScoreboardManager> entry : ParaPractice.scoreboard.entrySet()) {
            Player players = Bukkit.getPlayer(entry.getKey());
            PracticePlayer practicePlayers = PracticePlayer.getAccount(players);

            if(practicePlayers.isInQueue()) {
                entry.getValue().setQueueScoreboard();
            } else if(!practicePlayers.isInMatch()) {
                entry.getValue().setLobbyScoreboard();
            }
        }
    }
    public static void removeRankedMatch(PracticePlayer practicePlayer) {


        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().ranked().containsKey(practicePlayer)) {
                ladder.getLadderType().ranked().remove(practicePlayer);
                ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).refresh();
            }
        }


        for(Map.Entry<UUID, ScoreboardManager> entry : ParaPractice.scoreboard.entrySet()) {
            Player players = Bukkit.getPlayer(entry.getKey());
            PracticePlayer practicePlayers = PracticePlayer.getAccount(players);

            if(practicePlayers.isInQueue()) {
                entry.getValue().setQueueScoreboard();
            } else if(!practicePlayers.isInMatch()) {
                entry.getValue().setLobbyScoreboard();
            }
        }
    }
    public static PracticePlayer getEnemy(PracticePlayer practicePlayer) {
        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().unranked().containsKey(practicePlayer)) {
                return ladder.getLadderType().unranked().get(practicePlayer);
            }
        }
        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().ranked().containsKey(practicePlayer)) {
                return ladder.getLadderType().ranked().get(practicePlayer);
            }
        }
        return null;
    }

    public static boolean isRanked(PracticePlayer practicePlayer) {
        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().ranked().containsKey(practicePlayer)) {
                return true;
            }
        }
        return false;
    }

    public static Ladder getMatchLadder(PracticePlayer practicePlayer) {
        for(Ladder ladder : Ladder.values()) {
            if(ladder.getLadderType().ranked().containsKey(practicePlayer)) {
                return ladder;
            } else if(ladder.getLadderType().unranked().containsKey(practicePlayer)) {
                return ladder;
            }
        }
        return null;
    }

    public static Map<PracticePlayer, PracticePlayer> getNoDebuffUnrankedMatches() {
        return noDebuffUnrankedMatches;
    }

    public static Map<PracticePlayer, PracticePlayer> getDebuffUnrankedMatches() {
        return debuffUnrankedMatches;
    }

    public static Map<PracticePlayer, PracticePlayer> getNoDebuffRankedMatches() {
        return noDebuffRankedMatches;
    }

    public static Map<PracticePlayer, PracticePlayer> getDebuffRankedMatches() {
        return debuffRankedMatches;
    }


    public static Map<PracticePlayer, PracticePlayer> getSumoUnrankedMatches() {
        return sumoUnrankedMatches;
    }

    public static Map<PracticePlayer, PracticePlayer> getSumoRankedMatches() {
        return sumoRankedMatches;
    }

    public static Map<PracticePlayer, PracticePlayer> getAxeUnrankedMatches() {
        return axeUnrankedMatches;
    }

    public static Map<PracticePlayer, PracticePlayer> getAxeRankedMatches() {
        return axeRankedMatches;
    }


    public static Map<PracticePlayer, PracticePlayer> getArcherUnrankedMatches() {
        return archerUnrankedMatches;
    }

    public static Map<PracticePlayer, PracticePlayer> getArcherRankedMatches() {
        return archerRankedMatches;
    }
}
