package fr.comore.paramen.practice.elos;

import fr.comore.paramen.practice.ladders.Ladder;
import fr.comore.paramen.rank.ParaRank;

import java.sql.SQLException;
import java.util.UUID;

public class ELOManager {

    public static void recalculeGlobalElo(UUID uuid) {
        long elos = 0;
        for(Ladder ladder : Ladder.values()) {
            elos += getLadderElo(ladder, uuid);
        }
        setGlobalElo(uuid, elos / Ladder.values().length);
    }

    public static long getGlobalElo(UUID uuid) {
        return (long) ParaRank.getInstance().getMySQL().query("SELECT global_elo FROM elos WHERE player_uuid='" +uuid.toString()+ "'", resultSet -> {
            try {
                if(resultSet.next()) {
                    return resultSet.getLong("global_elo");
                }
            } catch (SQLException e) {
                return 1000;
            }
            return 1000;
        });
    }
    public static long getLadderElo(Ladder ladder, UUID uuid) {
        String tableName = "elo_" + ladder.getName().toLowerCase();
        return (long) ParaRank.getInstance().getMySQL().query("SELECT "+tableName+" FROM elos WHERE player_uuid='" +uuid.toString()+ "'", resultSet -> {
            try {
                if(resultSet.next()) {
                    return resultSet.getLong(tableName);
                }
            } catch (SQLException e) {
                return 1000;
            }
            return 1000;
        });
    }



    public static void setGlobalElo(UUID uuid, long elo) {
        ParaRank.getInstance().getMySQL().update("UPDATE elos SET global_elo = " + elo + " WHERE player_uuid='"+uuid.toString()+"'");
    }

    public static void setPremiumElo(UUID uuid, long elo) {
        ParaRank.getInstance().getMySQL().update("UPDATE elos SET premium_global = " + elo + " WHERE player_uuid='"+uuid.toString()+"'");
    }

    public static void setLadderElo(Ladder ladder, long elo, UUID uuid) {
        String colonneName = "elo_"+ladder.getName().toLowerCase();
        ParaRank.getInstance().getMySQL().update("UPDATE elos SET " + colonneName + " = " + elo + " WHERE player_uuid='"+uuid.toString()+"'");
    }


    public static void addLadderElo(Ladder ladder, UUID uuid, long elo) {
        setLadderElo(ladder, getLadderElo(ladder, uuid) + elo, uuid);
    }

    public static void removeLadderElo(Ladder ladder, UUID uuid, long elo) {
        setLadderElo(ladder, getLadderElo(ladder, uuid) - elo, uuid);
    }
}
