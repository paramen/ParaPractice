package fr.comore.paramen.practice.listeners;

import fr.comore.paramen.practice.DuelManager;
import fr.comore.paramen.practice.elos.ELOManager;
import fr.comore.paramen.practice.inventory.InventoryManager;
import fr.comore.paramen.practice.matchs.MatchManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.spectator.SpectatorManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.util.Map;

public class KillEvent implements Listener {

    @EventHandler
    public void onKill(EntityDamageEvent event) {
        if(!(event.getEntity() instanceof Player)) return;
        Player player = (Player) event.getEntity();
        PracticePlayer practicePlayer = PracticePlayer.getAccount(player);
        Damageable damageable = player;
        double integer = event.getFinalDamage();
        if(integer >= damageable.getHealth()) {
            damageable.setHealth(20);
            if(practicePlayer.isInMatch()) {
                if (DuelManager.duelers.containsKey(practicePlayer)) {
                    DuelManager.duelers.remove(practicePlayer);
                } else if(DuelManager.duelers.containsKey(MatchManager.getEnemy(practicePlayer))) {
                    DuelManager.duelers.remove(MatchManager.getEnemy(practicePlayer));
                }
                if(MatchManager.isRanked(practicePlayer)) {
                    InventoryManager.addInventory(practicePlayer);
                    InventoryManager.addInventory(MatchManager.getEnemy(practicePlayer));

                    ELOManager.removeLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId(), 5);
                    ELOManager.addLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId(), 5);
                    player.sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                    MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                    player.sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                    MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                    practicePlayer.sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                    MatchManager.getEnemy(practicePlayer).sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), practicePlayer.getPlayer().getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                    for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                        if(spec.getValue() == practicePlayer) {
                            spec.getKey().showAllPlayers();
                            spec.getKey().setLobbyItems();
                            spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                            spec.getKey().getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                            spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                            SpectatorManager.removeSpectator(spec.getKey());
                        }
                    }
                    for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                        if(spec.getValue() == MatchManager.getEnemy(practicePlayer)) {
                            spec.getKey().showAllPlayers();
                            spec.getKey().setLobbyItems();
                            spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                            spec.getKey().getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                            spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                            SpectatorManager.removeSpectator(spec.getKey());
                        }
                    }
                    practicePlayer.setLobbyItems();
                    ELOManager.recalculeGlobalElo(player.getUniqueId());
                    ELOManager.recalculeGlobalElo(MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId());
                    MatchManager.getEnemy(practicePlayer).setLobbyItems();
                    practicePlayer.showAllPlayers();
                    MatchManager.getEnemy(practicePlayer).showAllPlayers();
                    practicePlayer.getPlayer().setHealth(20D);
                    MatchManager.getEnemy(practicePlayer).getPlayer().setHealth(20D);

                    MatchManager.removeRankedMatch(MatchManager.getEnemy(practicePlayer));
                    MatchManager.removeRankedMatch(practicePlayer);
                } else {
                    InventoryManager.addInventory(practicePlayer);
                    InventoryManager.addInventory(MatchManager.getEnemy(practicePlayer));

                    player.sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                    MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                    player.sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                    MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                    practicePlayer.sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName()+"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                    MatchManager.getEnemy(practicePlayer).sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName()+"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                    for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                        if(spec.getValue() == practicePlayer) {
                            spec.getKey().showAllPlayers();
                            spec.getKey().setLobbyItems();
                            spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                            spec.getKey().getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                            spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName()+"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                            SpectatorManager.removeSpectator(spec.getKey());
                        }
                    }
                    for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                        if(spec.getValue() == MatchManager.getEnemy(practicePlayer)) {
                            spec.getKey().showAllPlayers();
                            spec.getKey().setLobbyItems();
                            spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                            spec.getKey().getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                            spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName()+"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                            SpectatorManager.removeSpectator(spec.getKey());
                        }
                    }

                    practicePlayer.setLobbyItems();
                    MatchManager.getEnemy(practicePlayer).setLobbyItems();
                    practicePlayer.showAllPlayers();
                    MatchManager.getEnemy(practicePlayer).showAllPlayers();
                    practicePlayer.getPlayer().setHealth(20D);
                    MatchManager.getEnemy(practicePlayer).getPlayer().setHealth(20D);


                    MatchManager.removeUnrankedMatch(MatchManager.getEnemy(practicePlayer));
                    MatchManager.removeUnrankedMatch(practicePlayer);
                }
            }
        } else {

        }
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        PracticePlayer.getAccount(event.getPlayer()).setLobbyItems();
    }



}
