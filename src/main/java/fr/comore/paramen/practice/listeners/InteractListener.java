package fr.comore.paramen.practice.listeners;

import fr.comore.paramen.practice.DuelManager;
import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.practice.SettingsManager;
import fr.comore.paramen.practice.elos.ELOManager;
import fr.comore.paramen.practice.kits.KitManager;
import fr.comore.paramen.practice.ladders.Ladder;
import fr.comore.paramen.practice.matchs.MatchManager;
import fr.comore.paramen.practice.party.PartyManager;
import fr.comore.paramen.practice.pearlmanager.PearlManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.queue.QueueManager;
import fr.comore.paramen.practice.spectator.SpectatorManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Door;

import java.util.Map;

public class InteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if(ParaPractice.getInstance().getAccounts().contains(PracticePlayer.getAccount(player))) {
            PracticePlayer practicePlayer = PracticePlayer.getAccount(player);
            if(event.getItem() == null || !event.getItem().hasItemMeta() || event.getItem().getItemMeta().getDisplayName() == null) return;


            if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {


                if(!practicePlayer.isInMatch()) {
                    if(event.getClickedBlock() == null || !(event.getClickedBlock().getState() instanceof Sign)) {

                        switch (event.getMaterial()) {
                            case STONE_SWORD:
                                practicePlayer.openUnrankedInventory();
                                break;
                            case IRON_SWORD:
                                practicePlayer.openRankedInventory();
                                break;
                            case REDSTONE:
                                if(event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7Leave queue")) {
                                    new QueueManager().removeRankedQueue(practicePlayer);
                                    new QueueManager().removeUnrankedQueue(practicePlayer);
                                    player.sendMessage("§eYou were removed from the queue.");
                                    practicePlayer.setLobbyItems();
                                } else if(event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7Stop spectate")) {
                                    SpectatorManager.removeSpectator(practicePlayer);
                                    player.sendMessage("§eYou're stopping spectating.");
                                    practicePlayer.setLobbyItems();
                                }
                                break;

                            case NETHER_STAR:
                                if(event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§7Leave Party")) {

                                    if(PartyManager.hasParty(practicePlayer)) {
                                        PartyManager.deleteParty(practicePlayer);
                                        practicePlayer.getPlayer().sendMessage("§eYour party was disbanded.");
                                    } else if(PartyManager.isInParty(practicePlayer)) {
                                        PartyManager.quitParty(practicePlayer);
                                    }

                                    practicePlayer.setLobbyItems();
                                }
                                break;

                            case NAME_TAG:
                                PartyManager.createParty(practicePlayer);
                                practicePlayer.setLobbyItems();
                                break;

                            case BOOK:
                                practicePlayer.openEditorInventory();
                                break;

                            case EMERALD:
                                practicePlayer.openLeaderboardInventory();
                                break;

                            case WATCH:
                                practicePlayer.openSettingsInventory();
                                break;
                        }
                    }
                }
            }
        }

    }

    @EventHandler
    public void onBlockInteract(PlayerInteractEvent event) {
        if(event.getClickedBlock() != null && event.getClickedBlock().getState() != null) {
            if(event.getClickedBlock().getState() instanceof Sign) {
                Player player = event.getPlayer();
                PracticePlayer practicePlayer = PracticePlayer.getAccount(player);

                Sign sign = (Sign) event.getClickedBlock().getState();

                if(KitManager.editors.containsKey(practicePlayer)) {
                    if(sign.getLine(0).contains("[Reset]")) {
                        KitManager.editors.get(practicePlayer).getLadderType().give(player);
                        player.sendMessage("§dResetting...");
                        return;
                    } else if(sign.getLine(0).contains("[Save]")) {
                        KitManager.quit(practicePlayer, true);
                        return;
                    }
                }
            } else if(event.getClickedBlock().getState().getData() instanceof Door) {
                Player player = event.getPlayer();
                PracticePlayer practicePlayer = PracticePlayer.getAccount(player);
                if(KitManager.editors.containsKey(practicePlayer)) {
                    KitManager.quit(practicePlayer, false);
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onInteractWhenEditing(PlayerInteractEvent event) {
        if(KitManager.editors.containsKey(PracticePlayer.getAccount(event.getPlayer()))) {
            event.setCancelled(true);
        } else {
            if(event.getAction() == Action.RIGHT_CLICK_AIR ||event.getAction() == Action.RIGHT_CLICK_BLOCK) {

                if(event.getMaterial() == Material.ENDER_PEARL) {
                    if(PearlManager.isCooldowned(PracticePlayer.getAccount(event.getPlayer()))) {
                        event.setCancelled(true);
                        event.getPlayer().sendMessage("§eYou can't use enderpearls now.");
                    } else {
                        PearlManager.addPearler(PracticePlayer.getAccount(event.getPlayer()));
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInventory(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if(PracticePlayer.getAccount(player).isInMatch() || PracticePlayer.getAccount(player).isInBuildMode() || KitManager.editors.containsKey(PracticePlayer.getAccount(player))) return;

        event.setCancelled(true);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        PracticePlayer practicePlayer = PracticePlayer.getAccount(player);
        Inventory inventory = event.getClickedInventory();
        ItemStack itemStack = event.getCurrentItem();

        if(event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta() || event.getCurrentItem().getItemMeta().getDisplayName() == null) return;


        player.updateInventory();
        if(!practicePlayer.isInMatch()) {
            if(inventory.getName().contains("§eRanked queue")){
                Ladder ladder = Ladder.getByName(itemStack.getItemMeta().getDisplayName());
                if(ladder != null) {

                    player.closeInventory();
                    if(new QueueManager().canRankedMatch(ladder)) {
                        MatchManager.addRankedMatch(ladder, ladder.getLadderType().queueRanked().get(0), practicePlayer);
                        return;
                    }
                    new QueueManager().addRankedQueue(practicePlayer, ladder);
                    player.sendMessage("§eYou were added to the §dRanked " + ladder.getName() + " §equeue with §d" + ELOManager.getLadderElo(ladder, player.getUniqueId()) + " §eelo.");
                    ParaPractice.scoreboard.get(player.getUniqueId()).refresh();
                    practicePlayer.setQueueItems();
                }
            } else if(inventory.getName().contains("§aUnranked queue")) {
                Ladder ladder = Ladder.getByName(itemStack.getItemMeta().getDisplayName());
                if(ladder != null) {
                    player.closeInventory();
                    if(new QueueManager().canUnrankedMatch(ladder)) {
                        MatchManager.addUnrankedMatch(ladder, ladder.getLadderType().queueUnranked().get(0), practicePlayer);
                        return;
                    }
                    new QueueManager().addUnrankedQueue(practicePlayer, ladder);
                    player.sendMessage("§eYou were added to the §dUnranked " + ladder.getName() + " §equeue.");
                    ParaPractice.scoreboard.get(player.getUniqueId()).refresh();
                    practicePlayer.setQueueItems();
                }
            } else if(inventory.getName().contains("§2Kit Editor")) {
                Ladder ladder = Ladder.getByName(itemStack.getItemMeta().getDisplayName());
                if(ladder != null) {
                    KitManager.setKitEditingInventory(practicePlayer, ladder);
                }
                event.setCancelled(true);
            } else if(inventory.getName().contains("§9Duel ")) {
                event.setCancelled(true);
                player.closeInventory();
                if(Bukkit.getPlayer(inventory.getName().replace("§9Duel ", "")) == null) {
                    player.sendMessage("§eThis player is not online.");
                    return;
                }

                Ladder ladder = Ladder.getByName(itemStack.getItemMeta().getDisplayName());
                if(ladder != null) {
                    if(DuelManager.duelers.containsKey(practicePlayer)) {
                        DuelManager.duelers.remove(practicePlayer);
                    }
                    DuelManager.addDuel(ladder, practicePlayer, PracticePlayer.getAccount(Bukkit.getPlayer(inventory.getName().replace("§9Duel ", ""))));
                }
            } else if(inventory.getName().contains("§3Settings")) {
                SettingsManager.SettingsList settingsList = SettingsManager.SettingsList.getByItemStack(itemStack);
                if(settingsList != null) {
                    SettingsManager.setSetting(practicePlayer, !SettingsManager.getSetting(practicePlayer, settingsList), settingsList);
                    inventory.setItem(event.getSlot(), SettingsManager.getSetting(practicePlayer, settingsList) ? settingsList.getEnabledItemStack() : settingsList.getDisabledItemStack());

                    if(settingsList == SettingsManager.SettingsList.TOGGLE_SCOREBOARD) {
                        if(!SettingsManager.getSetting(practicePlayer, settingsList)) {
                            ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).clearScoreboard();
                        } else {
                            if(practicePlayer.isInMatch()) {
                                ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).setMatchScoreboard();
                            } else if(practicePlayer.isInQueue()) {
                                ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).setQueueScoreboard();
                            } else {
                                ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).setLobbyScoreboard();
                            }
                        }
                    }

                }
                event.setCancelled(true);
            }
        }

    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if(event.getEntity() instanceof Player) {
            PracticePlayer practicePlayer = PracticePlayer.getAccount((Player) event.getEntity());
            if(!practicePlayer.isInMatch()) {
                event.setCancelled(true);
            } else {
                if(MatchManager.getMatchLadder(practicePlayer).isSumoMode()) {
                    event.setDamage(0D);
                }
            }
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if(event.getDamager() instanceof Player) {
            if(SpectatorManager.isSpectating(PracticePlayer.getAccount((Player) event.getDamager()))) {
                event.setCancelled(true);
            }
        } else if(event.getDamager() instanceof Arrow) {
            Arrow arrow = (Arrow) event.getDamager();
            if(arrow.getShooter() instanceof Player && event.getEntity() instanceof Player) {
                Player player = (Player) arrow.getShooter();
                Player damaged = (Player) event.getEntity();
                player.sendMessage("§eYou've shooted " + damaged.getName() + " (§c"+(Math.round(damaged.getHealth() - event.getFinalDamage() / 2) == 0 ? "§cDied" : (Math.round(damaged.getHealth() - event.getFinalDamage() / 2)+"♥")+"§e)"));
            }
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        PracticePlayer practicePlayer = PracticePlayer.getAccount(event.getPlayer());
        Player player = event.getPlayer();
        if(practicePlayer.isInMatch()) {
            if (DuelManager.duelers.containsKey(practicePlayer)) {
                DuelManager.duelers.remove(practicePlayer);
            } else if(DuelManager.duelers.containsKey(MatchManager.getEnemy(practicePlayer))) {
                DuelManager.duelers.remove(MatchManager.getEnemy(practicePlayer));
            }
            if(MatchManager.getMatchLadder(practicePlayer).isSumoMode()) {
                Material material = event.getPlayer().getLocation().getBlock().getType();
                if (material == Material.STATIONARY_WATER || material == Material.WATER) {

                        if(MatchManager.isRanked(practicePlayer)) {
                            ELOManager.removeLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId(), 5);
                            ELOManager.addLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId(), 5);
                            player.sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                            MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                            player.sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                            MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                            practicePlayer.sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                            MatchManager.getEnemy(practicePlayer).sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), practicePlayer.getPlayer().getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");

                            for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                                if(spec.getValue() == practicePlayer) {
                                    spec.getKey().showAllPlayers();
                                    spec.getKey().setLobbyItems();
                                    spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                                    spec.getKey().getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                                    spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                                    SpectatorManager.removeSpectator(spec.getKey());
                                }
                            }
                            for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                                if(spec.getValue() == MatchManager.getEnemy(practicePlayer)) {
                                    spec.getKey().showAllPlayers();
                                    spec.getKey().setLobbyItems();
                                    spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                                    spec.getKey().getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                                    spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                                    SpectatorManager.removeSpectator(spec.getKey());
                                }
                            }

                            practicePlayer.setLobbyItems();
                            ELOManager.recalculeGlobalElo(player.getUniqueId());
                            ELOManager.recalculeGlobalElo(MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId());
                            MatchManager.getEnemy(practicePlayer).setLobbyItems();
                            practicePlayer.showAllPlayers();
                            MatchManager.getEnemy(practicePlayer).showAllPlayers();
                            practicePlayer.getPlayer().setHealth(20D);
                            MatchManager.getEnemy(practicePlayer).getPlayer().setHealth(20D);

                            MatchManager.removeRankedMatch(MatchManager.getEnemy(practicePlayer));
                            MatchManager.removeRankedMatch(practicePlayer);
                        } else {

                            player.sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                            MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                            player.sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                            MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                            practicePlayer.sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                            MatchManager.getEnemy(practicePlayer).sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");


                            for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                                if(spec.getValue() == practicePlayer) {
                                    spec.getKey().showAllPlayers();
                                    spec.getKey().setLobbyItems();
                                    spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                                    spec.getKey().getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                                    spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName()+"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                                    SpectatorManager.removeSpectator(spec.getKey());
                                }
                            }
                            for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                                if(spec.getValue() == MatchManager.getEnemy(practicePlayer)) {
                                    spec.getKey().showAllPlayers();
                                    spec.getKey().setLobbyItems();
                                    spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §ewas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§e.");
                                    spec.getKey().getPlayer().sendMessage("§eWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                                    spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName()+"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                                    SpectatorManager.removeSpectator(spec.getKey());
                                }
                            }

                            practicePlayer.setLobbyItems();
                            MatchManager.getEnemy(practicePlayer).setLobbyItems();
                            practicePlayer.showAllPlayers();
                            MatchManager.getEnemy(practicePlayer).showAllPlayers();
                            practicePlayer.getPlayer().setHealth(20D);
                            MatchManager.getEnemy(practicePlayer).getPlayer().setHealth(20D);
                            MatchManager.removeUnrankedMatch(MatchManager.getEnemy(practicePlayer));
                            MatchManager.removeUnrankedMatch(practicePlayer);
                        }
                }
            }
        }
        }
}


