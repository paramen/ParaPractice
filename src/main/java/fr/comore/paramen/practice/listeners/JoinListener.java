package fr.comore.paramen.practice.listeners;

import fr.comore.paramen.practice.DuelManager;
import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.practice.elos.ELOManager;
import fr.comore.paramen.practice.inventory.InventoryManager;
import fr.comore.paramen.practice.kits.KitManager;
import fr.comore.paramen.practice.matchs.MatchManager;
import fr.comore.paramen.practice.party.PartyManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.scoreboard.ScoreboardManager;
import fr.comore.paramen.practice.spectator.SpectatorManager;
import fr.comore.paramen.rank.accounts.Account;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Map;
import java.util.UUID;

public class JoinListener implements Listener {


    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        event.setJoinMessage(null);



        PracticePlayer practicePlayer = new PracticePlayer(event.getPlayer());
        Player player = event.getPlayer();
        Account account = new Account(player);
        account.setup();
        practicePlayer.setup().setLobbyItems();
        new ScoreboardManager(player).init();
        for(Map.Entry<UUID, ScoreboardManager> entry : ParaPractice.scoreboard.entrySet()) {
            Player players = Bukkit.getPlayer(entry.getKey());
            PracticePlayer practicePlayers = PracticePlayer.getAccount(players);

            if(practicePlayers.isInQueue()) {
                entry.getValue().setQueueScoreboard();
            } else if(!practicePlayers.isInMatch()) {
                entry.getValue().setLobbyScoreboard();
            }
        }

        for(Player players : Bukkit.getServer().getOnlinePlayers()) {
            if(PracticePlayer.getAccount(players).isInMatch()) {
                players.hidePlayer(player);
            }
        }
        ELOManager.recalculeGlobalElo(player.getUniqueId());
        player.setHealth(20);
        player.teleport(Bukkit.getWorld("world").getSpawnLocation());
        for(int i = 0; i < 100; i++) {
            player.sendMessage(" ");
        }
        player.sendMessage("§d§lWelcome to Paramen Practice.");
        player.sendMessage("§7§m------------------------------------------------");
        player.sendMessage("§fThanks to the players who support the §fserver by §fmaking");
        player.sendMessage("§fdonations §fon the shop!");
        player.sendMessage(" ");
        player.sendMessage("§fIt's thanks to them that all the other §fplayers ");
        player.sendMessage("§fcan play §ffor §ffree §fand that we can keep the server alive.");
        player.sendMessage(" ");
        player.sendMessage("§dparamen.eu");
        player.sendMessage("§7§m------------------------------------------------");

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        Player player = event.getPlayer();
        PracticePlayer practicePlayer = PracticePlayer.getAccount(event.getPlayer());

        for(Map.Entry<UUID, ScoreboardManager> entry : ParaPractice.scoreboard.entrySet()) {
            Player players = Bukkit.getPlayer(entry.getKey());
            PracticePlayer practicePlayers = PracticePlayer.getAccount(players);

            if(practicePlayers.isInQueue()) {
                entry.getValue().setQueueScoreboard(true);
            } else if(!practicePlayers.isInMatch()) {
                entry.getValue().setLobbyScoreboard(true);
            }
        }

        if(PartyManager.hasParty(practicePlayer)) {
            PartyManager.deleteParty(practicePlayer);
        } else if(PartyManager.isInParty(practicePlayer)) {
            PartyManager.quitParty(practicePlayer);
        }

        if(practicePlayer.isInMatch()) {
            if (DuelManager.duelers.containsKey(practicePlayer)) {
                DuelManager.duelers.remove(practicePlayer);
            } else if(DuelManager.duelers.containsKey(MatchManager.getEnemy(practicePlayer))) {
                DuelManager.duelers.remove(MatchManager.getEnemy(practicePlayer));
            }
            if(MatchManager.isRanked(practicePlayer)) {
                InventoryManager.addInventory(practicePlayer);
                InventoryManager.addInventory(MatchManager.getEnemy(practicePlayer));

                ELOManager.removeLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId(), 5);
                ELOManager.addLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId(), 5);
                player.sendMessage("§d"+player.getName() + " §dwas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§d.");
                MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§d"+player.getName() + " §dwas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§d.");
                player.sendMessage("§dWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§dWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                practicePlayer.sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                MatchManager.getEnemy(practicePlayer).sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), practicePlayer.getPlayer().getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                    if(spec.getValue() == practicePlayer) {
                        spec.getKey().showAllPlayers();
                        spec.getKey().setLobbyItems();
                        spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §dwas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§d.");
                        spec.getKey().getPlayer().sendMessage("§dWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                        spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                        SpectatorManager.removeSpectator(spec.getKey());
                    }
                }
                for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                    if(spec.getValue() == MatchManager.getEnemy(practicePlayer)) {
                        spec.getKey().showAllPlayers();
                        spec.getKey().setLobbyItems();
                        spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §dwas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§d.");
                        spec.getKey().getPlayer().sendMessage("§dWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                        spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId()) + ")" +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() + " (" + ELOManager.getLadderElo(MatchManager.getMatchLadder(practicePlayer), player.getUniqueId()) + ")" +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                        SpectatorManager.removeSpectator(spec.getKey());
                    }
                }
                practicePlayer.setLobbyItems();
                ELOManager.recalculeGlobalElo(player.getUniqueId());
                ELOManager.recalculeGlobalElo(MatchManager.getEnemy(practicePlayer).getPlayer().getUniqueId());
                MatchManager.getEnemy(practicePlayer).setLobbyItems();
                practicePlayer.showAllPlayers();
                MatchManager.getEnemy(practicePlayer).showAllPlayers();
                practicePlayer.getPlayer().setHealth(20D);
                MatchManager.getEnemy(practicePlayer).getPlayer().setHealth(20D);

                MatchManager.removeRankedMatch(MatchManager.getEnemy(practicePlayer));
                MatchManager.removeRankedMatch(practicePlayer);
            } else {
                InventoryManager.addInventory(practicePlayer);
                InventoryManager.addInventory(MatchManager.getEnemy(practicePlayer));

                player.sendMessage("§d"+player.getName() + " §dwas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§d.");
                MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§d"+player.getName() + " §dwas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§d.");
                player.sendMessage("§dWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                MatchManager.getEnemy(practicePlayer).getPlayer().sendMessage("§dWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                practicePlayer.sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName() +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                MatchManager.getEnemy(practicePlayer).sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName()  +"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName() +"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                    if(spec.getValue() == practicePlayer) {
                        spec.getKey().showAllPlayers();
                        spec.getKey().setLobbyItems();
                        spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §dwas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§d.");
                        spec.getKey().getPlayer().sendMessage("§dWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                        spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName()+"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                        SpectatorManager.removeSpectator(spec.getKey());
                    }
                }
                for(Map.Entry<PracticePlayer, PracticePlayer> spec : SpectatorManager.getSpectators().entrySet()) {
                    if(spec.getValue() == MatchManager.getEnemy(practicePlayer)) {
                        spec.getKey().showAllPlayers();
                        spec.getKey().setLobbyItems();
                        spec.getKey().getPlayer().sendMessage("§d"+player.getName() + " §dwas slain by §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"§d.");
                        spec.getKey().getPlayer().sendMessage("§dWinner: §d"+MatchManager.getEnemy(practicePlayer).getPlayer().getName());
                        spec.getKey().sendJSONMessage("[\"\",{\"text\":\"Inventories: \",\"color\":\"yellow\"},{\"text\":\""+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+MatchManager.getEnemy(practicePlayer).getPlayer().getName()+"\"}},{\"text\":\", \"},{\"text\":\""+player.getName()+"\",\"color\":\"red\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/inv "+player.getName()+"\"}}]");
                        SpectatorManager.removeSpectator(spec.getKey());
                    }
                }
                practicePlayer.setLobbyItems();
                MatchManager.getEnemy(practicePlayer).setLobbyItems();
                practicePlayer.showAllPlayers();
                MatchManager.getEnemy(practicePlayer).showAllPlayers();
                practicePlayer.getPlayer().setHealth(20D);
                MatchManager.getEnemy(practicePlayer).getPlayer().setHealth(20D);

                MatchManager.removeUnrankedMatch(MatchManager.getEnemy(practicePlayer));
                MatchManager.removeUnrankedMatch(practicePlayer);
            }
        }

        if(ParaPractice.scoreboard.containsKey(player.getUniqueId())) {
            ParaPractice.scoreboard.remove(player.getUniqueId());
        }
        practicePlayer.delete();
        if(KitManager.editors.containsKey(practicePlayer)) {
            KitManager.quit(practicePlayer, false);
        }
    }

}
