package fr.comore.paramen.practice.listeners;

import fr.comore.paramen.practice.player.PracticePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class BuildEvent implements Listener {

    @EventHandler
    public void onBuild(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        PracticePlayer practicePlayer = PracticePlayer.getAccount(player);
        if(practicePlayer.isInBuildMode()) {
            event.setCancelled(false);
            return;
        }
        event.setCancelled(true);
    }
    @EventHandler
    public void onPlace(BlockBreakEvent event) {
        Player player = event.getPlayer();
        PracticePlayer practicePlayer = PracticePlayer.getAccount(player);
        if(practicePlayer.isInBuildMode()) {
            event.setCancelled(false);
            return;
        }
        event.setCancelled(true);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        PracticePlayer practicePlayer = PracticePlayer.getAccount(player);
        if(!practicePlayer.isInMatch()){
            if(practicePlayer.isInBuildMode()) {
                event.setCancelled(false);
                return;
            }
            event.setCancelled(true);
        } else {
        }
    }
    @EventHandler
    public void onPickUp(PlayerPickupItemEvent event) {
        Player player = event.getPlayer();
        PracticePlayer practicePlayer = PracticePlayer.getAccount(player);
        if(practicePlayer.isInBuildMode()) {
            event.setCancelled(false);
            return;
        }
        event.setCancelled(true);

    }
}
