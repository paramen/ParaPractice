package fr.comore.paramen.practice.inventory;

import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.utils.ItemBuilder;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryManager {

    private static HashMap<PracticePlayer, ItemStack[]> inventories = new HashMap<>();

    public static void addInventory(PracticePlayer practicePlayer) {
        Inventory inventory = practicePlayer.getPlayer().getInventory();

        inventories.put(practicePlayer, inventory.getContents());

        ParaPractice.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(ParaPractice.getInstance(), () -> {
            inventories.remove(practicePlayer);
        }, 20 * 120);
    }

    public static Map<PracticePlayer, ItemStack[]> getInventories() {
        return inventories;
    }
}
