package fr.comore.paramen.practice;

import fr.comore.paramen.practice.ladders.Ladder;
import fr.comore.paramen.practice.player.PracticePlayer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class DuelManager {
    public static Map<PracticePlayer, PracticePlayer> duelers = new HashMap<>();
    public static Map<PracticePlayer, Ladder> ladders = new HashMap<>();

    public static void addDuel(Ladder ladder, PracticePlayer practicePlayer1, PracticePlayer practicePlayer2) {
        Player player1 = practicePlayer1.getPlayer();
        Player player2 = practicePlayer2.getPlayer();

        duelers.put(practicePlayer2, practicePlayer1);
        ladders.put(practicePlayer1, ladder);
        player1.sendMessage("§eSend a duel request to §d"+player2.getName()+" §ewith kit §d" + ladder.getName() + " §eon §d"+ladder.getArena().getName() + "§e.");

        TextComponent textComponent = new TextComponent("§d"+player1.getName() +"§e has sent you a duel request with kit §d"+ladder.getName()+"§e on §d"+ladder.getArena().getName()+"§e. §a[Accept]");
        textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§eAccept the request.").create()));
        textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/accept " + player1.getName()));

        player2.spigot().sendMessage(textComponent);
    }

    public static Ladder getLadder(PracticePlayer practicePlayer) {
        return ladders.get(practicePlayer);
    }

    public static boolean isDueled(PracticePlayer practicePlayer) {
        return duelers.containsValue(practicePlayer);
    }

    public static PracticePlayer getDueler(PracticePlayer practicePlayer) {
        for(Map.Entry<PracticePlayer, PracticePlayer> entry : duelers.entrySet()) {
            if(entry.getValue() == practicePlayer) {
                return entry.getKey();
            }
        }
        return null;
    }
}
