package fr.comore.paramen.practice.arena;


import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public enum Arena {
    SAND(1, "Sand", "Sidovac", new Location(Bukkit.getWorld("world"), 204.4693, 209, 257.9717, -134.9067f, 9.735582f), new Location(Bukkit.getWorld("world"), 283.34, 209, 204.5986, -46.904945f, 6.0719495f), 20),
    BRIDGE(2, "Bridge", "Sidovac", new Location(Bukkit.getWorld("world"), -195.7693, 227, -166.7643, -91.30345f, 1.193676f), new Location(Bukkit.getWorld("world"), -116.9721, 226, -166.309, 91.01453f, -0.33937758f), 20),
    MOUTAINS(3, "Mountains", "Sidovac", new Location(Bukkit.getWorld("world"), -188.4736, 206, 250.01341, 229.89804f, 3.0832973f), new Location(Bukkit.getWorld("world"), -125.4438, 206, 212.3864, 49.998363f, -1.7856164f), 20),
    SUMO(4, "Sumo", "Polux24", new Location(Bukkit.getWorld("world"), 200, 202, -171.8559, 1.1890146f, -7.6628966f), new Location(Bukkit.getWorld("world"), 200.5890, 202, -136.7049, -179.75496f, 0.28717893f), 20);

    private int id;
    private String name;
    private String author;
    private Location location1;
    private Location location2;
    private int maxPlayers;

    Arena(int id, String name, String author, Location location1, Location location2, int maxPlayers) {
        this.id = id;
        this.name = name;
        this.location1 = location1;
        this.location2 = location2;
        this.maxPlayers = maxPlayers;
        this.author = author;
    }

    public static Arena[] getArenas() {
        return values();
    }

    public static List<Arena> getArenasList() {
        List<Arena> list = new ArrayList<>();
        for(Arena arena : values()) {
            list.add(arena);
        }
        return list;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public Location getLocation1() {
        return location1;
    }

    public Location getLocation2() {
        return location2;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }
}
