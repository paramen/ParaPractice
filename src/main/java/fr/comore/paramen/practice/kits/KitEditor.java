package fr.comore.paramen.practice.kits;

import fr.comore.paramen.practice.ladders.Ladder;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.rank.ParaRank;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.io.IOException;
import java.sql.SQLException;

public class KitEditor {

    public static void setKit(Ladder ladder, PracticePlayer practicePlayer, PlayerInventory inventory) {
        String edited = "kit_"+ladder.getName();
        String bool = "edited_"+ladder.getName();
        ParaRank.getInstance().getMySQL().update("UPDATE kits SET " + edited + " = '" + InventoryStringDeSerializer.playerInventoryToBase64(inventory) + "', "+bool+" = 1 WHERE player_uuid='" + practicePlayer.getPlayer().getUniqueId().toString() + "'");
    }

    public static Inventory getKit(Ladder ladder, PracticePlayer practicePlayer) {
        String edited = "kit_"+ladder.getName();
        return (Inventory) ParaRank.getInstance().getMySQL().query("SELECT " + edited + " FROM kits WHERE player_uuid='"+practicePlayer.getPlayer().getUniqueId().toString()+"'", resultSet -> {
            try {
                if(resultSet.next()) {
                    try {
                        return InventoryStringDeSerializer.fromBase64(resultSet.getString(edited));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    public static void setup(PracticePlayer practicePlayer) {
        if(!hasCreated(practicePlayer)) {
            createLine(practicePlayer);
        }
    }

    public static void createLine(PracticePlayer practicePlayer) {

        ParaRank.getInstance().getMySQL().update("INSERT INTO kits (player_uuid) VALUES ('"+practicePlayer.getPlayer().getUniqueId().toString()+"')");
    }

    public static boolean hasEditedKit(Ladder ladder, PracticePlayer practicePlayer) {
        String edited = "edited_"+ladder.getName();
        return (boolean) ParaRank.getInstance().getMySQL().query("SELECT "+edited+" FROM kits WHERE player_uuid='"+practicePlayer.getPlayer().getUniqueId().toString()+"'", resultSet -> {
            try {
                if(resultSet.next()) {
                    return resultSet.getInt(edited) == 1;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return false;
        });
    }

    public static boolean hasCreated(PracticePlayer practicePlayer) {
        String edited = "edited_nodebuff";
        return (boolean) ParaRank.getInstance().getMySQL().query("SELECT "+edited+" FROM kits WHERE player_uuid='"+practicePlayer.getPlayer().getUniqueId().toString()+"'", resultSet -> {
            try {
                return resultSet.next();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return false;
        });
    }

    public static void setKitOnInventory(Ladder ladder, PracticePlayer practicePlayer) {
        Inventory inventory = getKit(ladder, practicePlayer);
        practicePlayer.getPlayer().getInventory().clear();
        practicePlayer.getPlayer().getInventory().setContents(inventory.getContents());
    }


}
