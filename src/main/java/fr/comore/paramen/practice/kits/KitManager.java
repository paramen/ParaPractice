package fr.comore.paramen.practice.kits;

import fr.comore.paramen.practice.ladders.Ladder;
import fr.comore.paramen.practice.ladders.LadderType;
import fr.comore.paramen.practice.player.PracticePlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class KitManager {

    public static HashMap<PracticePlayer, Ladder> editors = new HashMap<>();

    public static void setKitEditingInventory(PracticePlayer practicePlayer, Ladder ladder) {
        LadderType ladderType = ladder.getLadderType();
        Player player = practicePlayer.getPlayer();

        editors.put(practicePlayer, ladder);
        ladderType.give(player);
        player.teleport(new Location(Bukkit.getWorld("world"), -1.386894, 52, 78.3, -1.446184f, 16.462614f));
        player.sendMessage("§dYou are editing the §d"+ladder.getName() + "§d kit.");
    }

    public static void quit(PracticePlayer practicePlayer, boolean save) {
        if(save) {
            KitEditor.setKit(editors.get(practicePlayer), practicePlayer, practicePlayer.getPlayer().getInventory());
            practicePlayer.getPlayer().sendMessage("§dYou've saved the kit.");
            return;
        }

        practicePlayer.setLobbyItems();
        practicePlayer.getPlayer().sendMessage("§dYou've leaved the editing mode.");
        editors.remove(practicePlayer);
    }

}
