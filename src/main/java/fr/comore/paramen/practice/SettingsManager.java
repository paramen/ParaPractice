package fr.comore.paramen.practice;

import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.utils.ItemBuilder;
import fr.comore.paramen.rank.ParaRank;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;
import java.util.function.Function;

public class SettingsManager {

    public enum SettingsList {

        PRIVATE_MESSAGE("Private message status", 1, new ItemBuilder(Material.WRITTEN_BOOK).setName("§6Private message §8: §aEnabled").toItemStack(), new ItemBuilder(Material.WRITTEN_BOOK).setName("§6Private message §8: §cDisabled").toItemStack(), "PRIVATE_MESSAGE_STATUS"),
        TOGGLE_SCOREBOARD("Toggle Scoreboard", 1, new ItemBuilder(Material.EMPTY_MAP).setName("§6Toggle Sidebar §8: §aEnabled").toItemStack(), new ItemBuilder(Material.EMPTY_MAP).setName("§6Toggle Sidebar §8: §cDisabled").toItemStack(), "TOGGLE_SCOREBOARD");

        private String name;
        private int id;
        private ItemStack enabledItemStack;
        private ItemStack disabledItemStack;
        private String shortcut_name;

        SettingsList(String name, int id, ItemStack enabledItemStack, ItemStack disabledItemStack, String shortcut_name) {
            this.name = name;
            this.id = id;
            this.enabledItemStack = enabledItemStack;
            this.disabledItemStack = disabledItemStack;
            this.shortcut_name = shortcut_name;
        }

        public static SettingsList getByItemStack(ItemStack itemStack) {
            for(SettingsList settingsList : SettingsList.values()) {
                if(settingsList.getEnabledItemStack().getItemMeta().getDisplayName().equalsIgnoreCase(itemStack.getItemMeta().getDisplayName()) || settingsList.getDisabledItemStack().getItemMeta().getDisplayName().equalsIgnoreCase(itemStack.getItemMeta().getDisplayName())) {
                    return settingsList;
                }
            }
            return null;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }

        public ItemStack getEnabledItemStack() {
            return enabledItemStack;
        }

        public ItemStack getDisabledItemStack() {
            return disabledItemStack;
        }

        public String getShortcut_name() {
            return shortcut_name;
        }
    }

    public static void setSetting(PracticePlayer practicePlayer, boolean enabled, SettingsList settingsList) {
        String name = settingsList.shortcut_name;
        int isEnabled = enabled == true ? 1 : 0;
        ParaRank.getInstance().getMySQL().update("UPDATE settings SET " + name + " = "+isEnabled + " WHERE player_uuid='"+practicePlayer.getPlayer().getUniqueId().toString()+"'");
    }

    public static boolean getSetting(PracticePlayer practicePlayer, SettingsList settingsList) {
        String name = settingsList.shortcut_name;
        return (boolean) ParaRank.getInstance().getMySQL().query("SELECT " + name + " FROM settings WHERE player_uuid='"+practicePlayer.getPlayer().getUniqueId().toString()+"'", resultSet -> {
            try {
                if(resultSet.next()) {
                    return resultSet.getBoolean(name);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return false;
        });
    }

    public static void setup(PracticePlayer practicePlayer) {
        if(!hasCreated(practicePlayer)) {
            ParaRank.getInstance().getMySQL().update("INSERT INTO settings (player_uuid) VALUES ('"+practicePlayer.getPlayer().getUniqueId().toString()+"')");
        }
    }

    public static boolean hasCreated(PracticePlayer practicePlayer) {
        return (boolean) ParaRank.getInstance().getMySQL().query("SELECT PRIVATE_MESSAGE_STATUS FROM settings WHERE player_uuid='"+practicePlayer.getPlayer().getUniqueId().toString()+"'", resultSet -> {
            try {
                return resultSet.next();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return false;
        });
    }

}
