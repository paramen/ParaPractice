package fr.comore.paramen.practice.ladders;

import fr.comore.paramen.practice.kits.KitEditor;
import fr.comore.paramen.practice.matchs.MatchManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.queue.QueueManager;
import fr.comore.paramen.practice.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;
import java.util.Map;

public class Axe extends LadderType {
    @Override
    public Ladder ladder() {
        return Ladder.AXE;
    }

    @Override
    public Map<PracticePlayer, PracticePlayer> unranked() {
        return MatchManager.getAxeUnrankedMatches();
    }

    @Override
    public Map<PracticePlayer, PracticePlayer> ranked() {
        return MatchManager.getAxeRankedMatches();
    }

    @Override
    public List<PracticePlayer> queueUnranked() {
        return new QueueManager().getAxeUnrankedQueue();
    }

    @Override
    public List<PracticePlayer> queueRanked() {
        return new QueueManager().getAxeRankedQueue();
    }


    @Override
    public void give(Player player) {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setArmorContents(null);
        playerInventory.setItem(0, new ItemBuilder(Material.IRON_AXE).addEnchant(Enchantment.DAMAGE_ALL, 1).toItemStack());
        playerInventory.setItem(1, new ItemBuilder(Material.GOLDEN_APPLE, 16).toItemStack());
        for(int i = 2; i < 8; i++) {
            playerInventory.setItem(i, new ItemBuilder(Material.POTION, 1, (short) 16421).toItemStack());
        }
        playerInventory.addItem(new ItemBuilder(Material.POTION, 1, (short) 8226).toItemStack());
        playerInventory.addItem(new ItemBuilder(Material.POTION, 1, (short) 8226).toItemStack());
        /*                CHANGEMENT ARMURE                  */
        playerInventory.setHelmet(new ItemBuilder(Material.IRON_HELMET).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());
        playerInventory.setChestplate(new ItemBuilder(Material.IRON_CHESTPLATE).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());
        playerInventory.setLeggings(new ItemBuilder(Material.IRON_LEGGINGS).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());
        playerInventory.setBoots(new ItemBuilder(Material.IRON_BOOTS).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());

        player.updateInventory();

        if(KitEditor.hasEditedKit(ladder(), PracticePlayer.getAccount(player))) {
            KitEditor.setKitOnInventory(ladder(), PracticePlayer.getAccount(player));
        }
    }
}
