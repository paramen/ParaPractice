package fr.comore.paramen.practice.ladders;

import fr.comore.paramen.practice.matchs.MatchManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.queue.QueueManager;
import fr.comore.paramen.practice.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;
import java.util.Map;

public class Sumo extends LadderType {
    @Override
    public Ladder ladder() {
        return Ladder.SUMO;
    }

    @Override
    public Map<PracticePlayer, PracticePlayer> unranked() {
        return MatchManager.getSumoUnrankedMatches();
    }

    @Override
    public Map<PracticePlayer, PracticePlayer> ranked() {
        return MatchManager.getSumoRankedMatches();
    }

    @Override
    public List<PracticePlayer> queueUnranked() {
        return new QueueManager().getSumoUnrankedQueue();
    }

    @Override
    public List<PracticePlayer> queueRanked() {
        return new QueueManager().getSumoRankedQueue();
    }


    @Override
    public void give(Player player) {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setArmorContents(null);

        player.updateInventory();
    }
}
