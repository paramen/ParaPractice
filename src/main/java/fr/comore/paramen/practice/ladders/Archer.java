package fr.comore.paramen.practice.ladders;

import fr.comore.paramen.practice.kits.KitEditor;
import fr.comore.paramen.practice.matchs.MatchManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.queue.QueueManager;
import fr.comore.paramen.practice.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;
import java.util.Map;

public class Archer extends LadderType {
    @Override
    public Ladder ladder() {
        return Ladder.ARCHER;
    }

    @Override
    public Map<PracticePlayer, PracticePlayer> unranked() {
        return MatchManager.getArcherUnrankedMatches();
    }

    @Override
    public Map<PracticePlayer, PracticePlayer> ranked() {
        return MatchManager.getArcherRankedMatches();
    }

    @Override
    public List<PracticePlayer> queueUnranked() {
        return new QueueManager().getArcherUnrankedQueue();
    }

    @Override
    public List<PracticePlayer> queueRanked() {
        return new QueueManager().getArcherRankedQueue();
    }

    @Override
    public void give(Player player) {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setArmorContents(null);

        playerInventory.setItem(0, new ItemBuilder(Material.BOW).addEnchant(Enchantment.ARROW_DAMAGE, 1).addEnchant(Enchantment.ARROW_INFINITE, 1).toItemStack());
        playerInventory.setItem(1, new ItemBuilder(Material.WOOD_PICKAXE).toItemStack());
        playerInventory.setItem(2, new ItemBuilder(Material.GOLDEN_APPLE, 12).toItemStack());
        playerInventory.setItem(7, new ItemBuilder(Material.POTION, 1, (short) 16454).toItemStack());
        playerInventory.setItem(8, new ItemBuilder(Material.COOKED_BEEF, 64).toItemStack());
        playerInventory.setItem(9, new ItemBuilder(Material.ARROW).toItemStack());

        playerInventory.setHelmet(new ItemBuilder(Material.LEATHER_HELMET).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());
        playerInventory.setChestplate(new ItemBuilder(Material.LEATHER_CHESTPLATE).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());
        playerInventory.setLeggings(new ItemBuilder(Material.LEATHER_LEGGINGS).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());
        playerInventory.setBoots(new ItemBuilder(Material.LEATHER_BOOTS).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());

        if(KitEditor.hasEditedKit(ladder(), PracticePlayer.getAccount(player))) {
            KitEditor.setKitOnInventory(ladder(), PracticePlayer.getAccount(player));
        }
    }
}
