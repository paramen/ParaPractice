package fr.comore.paramen.practice.ladders;

import fr.comore.paramen.practice.player.PracticePlayer;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;

public abstract class LadderType {
    public abstract Ladder ladder();
    public abstract Map<PracticePlayer, PracticePlayer> unranked();
    public abstract Map<PracticePlayer, PracticePlayer> ranked();
    public abstract List<PracticePlayer> queueUnranked();
    public abstract List<PracticePlayer> queueRanked();
    public abstract void give(Player player);
}
