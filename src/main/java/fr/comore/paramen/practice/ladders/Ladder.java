package fr.comore.paramen.practice.ladders;

import fr.comore.paramen.practice.arena.Arena;
import fr.comore.paramen.practice.utils.ItemBuilder;
import fr.comore.paramen.rank.ranks.RankUnit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public enum Ladder {
    NODEBUFF("NoDebuff", 1, 1500, new NoDebuff(), new ItemBuilder(Material.POTION, 1, (short) 16421).setName("§dNoDebuff").toItemStack(), Arena.MOUTAINS, false, true),
    DEBUFF("Debuff", 2, 1500, new Debuff(), new ItemBuilder(Material.POTION, 1, (short) 16420).setName("§aDebuff").toItemStack(), Arena.MOUTAINS, false, true),
    AXE("Axe", 3, 1500, new Axe(), new ItemBuilder(Material.STONE_AXE, 1, (short) 16420).setUnbreakable(true).setName("§dAxe").toItemStack(), Arena.SAND, false, true),
    SUMO("Sumo", 4, 1500, new Sumo(), new ItemBuilder(Material.SLIME_BALL, 1).setName("§dSumo").toItemStack(), Arena.SUMO, true, false),
    ARCHER("Archer", 5, 1500, new Archer(), new ItemBuilder(Material.BOW).setName("§dArcher").toItemStack(), Arena.BRIDGE, false, true);

    private String name;
    private int id;
    private int startelo;
    private LadderType ladderType;
    private ItemStack itemStack;
    private Arena arena;
    private boolean sumoMode;
    private boolean editable;

    private Ladder(String name, int id, int startelo, LadderType ladderType, ItemStack itemStack, Arena arena, boolean sumoMode, boolean editable) {
        this.name = name;
        this.id = id;
        this.startelo = startelo;
        this.itemStack = itemStack;
        this.ladderType = ladderType;
        this.arena = arena;
        this.sumoMode = sumoMode;
        this.editable = editable;
    }

    public static Ladder getByName(String name) {
        return Arrays.stream(values()).filter(l -> name.contains(l.getName())).findAny().orElse(null);
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getStartelo() {
        return startelo;
    }

    public LadderType getLadderType() {
        return ladderType;
    }

    public Arena getArena() {
        return arena;
    }

    public boolean isSumoMode() {
        return sumoMode;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public boolean isEditable() {
        return editable;
    }
}
