package fr.comore.paramen.practice.ladders;

import fr.comore.paramen.practice.kits.KitEditor;
import fr.comore.paramen.practice.matchs.MatchManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.practice.queue.QueueManager;
import fr.comore.paramen.practice.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;
import java.util.Map;

public class NoDebuff extends LadderType {
    @Override
    public Ladder ladder() {
        return Ladder.NODEBUFF;
    }

    @Override
    public Map<PracticePlayer, PracticePlayer> unranked() {
        return MatchManager.getNoDebuffUnrankedMatches();
    }

    @Override
    public Map<PracticePlayer, PracticePlayer> ranked() {
        return MatchManager.getNoDebuffRankedMatches();
    }

    @Override
    public List<PracticePlayer> queueUnranked() {
        return new QueueManager().getNoDebuffUnrankedQueue();
    }

    @Override
    public List<PracticePlayer> queueRanked() {
        return new QueueManager().getNoDebuffRankedQueue();
    }

    @Override
    public void give(Player player) {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setArmorContents(null);
        playerInventory.setItem(0, new ItemBuilder(Material.DIAMOND_SWORD).addEnchant(Enchantment.DAMAGE_ALL, 1).addEnchant(Enchantment.FIRE_ASPECT, 2).toItemStack());
        playerInventory.setItem(1, new ItemBuilder(Material.ENDER_PEARL, 16).toItemStack());
        for(int i = 4; i < 36; i++) {
            playerInventory.setItem(i, new ItemBuilder(Material.POTION, 1, (short) 16421).toItemStack());
        }
        playerInventory.setItem(8, new ItemBuilder(Material.COOKED_BEEF, 64).toItemStack());
        playerInventory.setItem(2, new ItemBuilder(Material.POTION, 1, (short) 8226).toItemStack());
        playerInventory.setItem(3, new ItemBuilder(Material.POTION, 1, (short) 8291).toItemStack());
        playerInventory.setItem(17, new ItemBuilder(Material.POTION, 1, (short) 8226).toItemStack());
        playerInventory.setItem(26, new ItemBuilder(Material.POTION, 1, (short) 8226).toItemStack());
        playerInventory.setItem(35, new ItemBuilder(Material.POTION, 1, (short) 8226).toItemStack());
        /*                CHANGEMENT ARMURE                  */
        playerInventory.setHelmet(new ItemBuilder(Material.DIAMOND_HELMET).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());
        playerInventory.setChestplate(new ItemBuilder(Material.DIAMOND_CHESTPLATE).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());
        playerInventory.setLeggings(new ItemBuilder(Material.DIAMOND_LEGGINGS).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());
        playerInventory.setBoots(new ItemBuilder(Material.DIAMOND_BOOTS).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).toItemStack());

        player.updateInventory();

        if(KitEditor.hasEditedKit(ladder(), PracticePlayer.getAccount(player))) {
            System.out.println("lol");
            KitEditor.setKitOnInventory(ladder(), PracticePlayer.getAccount(player));
        }
    }
}
