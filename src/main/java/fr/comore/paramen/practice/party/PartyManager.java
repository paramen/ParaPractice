package fr.comore.paramen.practice.party;

import fr.comore.paramen.practice.ParaPractice;
import fr.comore.paramen.practice.player.PracticePlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PartyManager {

    private static HashMap<PracticePlayer, List<PracticePlayer>> parties = new HashMap<>();

    public static void createParty(PracticePlayer practicePlayer) {
        parties.put(practicePlayer, new ArrayList<>());
        ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).setLobbyScoreboard();
        practicePlayer.getPlayer().sendMessage("§eYou have created a party.");
        practicePlayer.setLobbyItems();
    }

    public static void deleteParty(PracticePlayer practicePlayer) {
        if(!hasParty(practicePlayer)) {
            practicePlayer.getPlayer().sendMessage("§eYou are not leader of a party.");
            return;
        }

        for(PracticePlayer practicePlayers : getMembers(practicePlayer)) {
            practicePlayers.setLobbyItems("");
            ParaPractice.scoreboard.get(practicePlayers.getPlayer().getUniqueId()).setLobbyScoreboard("");
        }
        ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).setLobbyScoreboard("");
        practicePlayer.setLobbyItems("");
        parties.remove(practicePlayer);
    }

    public static void quitParty(PracticePlayer practicePlayer) {
        if(hasParty(practicePlayer)) {
            practicePlayer.getPlayer().sendMessage("§eYou cannot quit your party, your are the leader, you can disband it.");
            return;
        }

        if(!isInParty(practicePlayer)) {
            practicePlayer.getPlayer().sendMessage("§eYou are not in a party.");
            return;
        }


        for(PracticePlayer practicePlayers : getMembers(getPartyLeader(practicePlayer))) {

            ParaPractice.scoreboard.get(practicePlayers.getPlayer().getUniqueId()).setLobbyScoreboard();
        }
        parties.get(getPartyLeader(practicePlayer)).remove(practicePlayer);
        practicePlayer.setLobbyItems();
        practicePlayer.getPlayer().sendMessage("§eYou have leaved your party.");
        ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).setLobbyScoreboard();
    }

    public static boolean hasParty(PracticePlayer practicePlayer) {
        return parties.containsKey(practicePlayer);
    }

    public static PracticePlayer getPartyLeader(PracticePlayer practicePlayer) {
        if(!isInParty(practicePlayer)) return null;
        if(hasParty(practicePlayer)) return practicePlayer;

        for(Map.Entry<PracticePlayer, List<PracticePlayer>> list : parties.entrySet()) {
            if(list.getValue().contains(practicePlayer)) return list.getKey();
        }
        return null;
    }

    public static boolean isInParty(PracticePlayer practicePlayer) {
        for(Map.Entry<PracticePlayer, List<PracticePlayer>> list : parties.entrySet()) {
            if(list.getValue().contains(practicePlayer) || list.getKey() == practicePlayer) return true;
        }
        return false;
    }

    public static void joinParty(PracticePlayer practicePlayer, PracticePlayer leaderOfParty) {
        if(!hasParty(leaderOfParty)) {
            practicePlayer.getPlayer().sendMessage("§eThis player has not a party.");
            return;
        }

        if(isInParty(practicePlayer)) {
            practicePlayer.getPlayer().sendMessage("§eYou are in a party.");
            return;
        }

        parties.get(leaderOfParty).add(practicePlayer);
        ParaPractice.scoreboard.get(practicePlayer.getPlayer().getUniqueId()).setLobbyScoreboard();
        for(PracticePlayer practicePlayers : getMembers(leaderOfParty)) {

            ParaPractice.scoreboard.get(practicePlayers.getPlayer().getUniqueId()).setLobbyScoreboard();
        }
        practicePlayer.setLobbyItems();
    }

    public static List<PracticePlayer> getMembers(PracticePlayer practicePlayer) {
        if(hasParty(practicePlayer)) {
            return parties.get(practicePlayer);
        } else if(isInParty(practicePlayer)) {
            return parties.get(PartyManager.getPartyLeader(practicePlayer));
        }
        return new ArrayList<>();
    }

    public static HashMap<PracticePlayer, List<PracticePlayer>> getParties() {
        return parties;
    }
}
