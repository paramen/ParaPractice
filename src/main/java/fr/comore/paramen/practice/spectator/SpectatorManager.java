package fr.comore.paramen.practice.spectator;

import fr.comore.paramen.practice.matchs.MatchManager;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.rank.accounts.Account;
import fr.comore.paramen.rank.ranks.RankUnit;

import java.util.HashMap;

public class SpectatorManager {
    private static HashMap<PracticePlayer, PracticePlayer> spectators = new HashMap<>();

    public static void addSpectator(PracticePlayer practicePlayer1, PracticePlayer practicePlayer2) {
        spectators.put(practicePlayer1, practicePlayer2);
        practicePlayer1.getPlayer().teleport(practicePlayer2.getPlayer().getLocation());
        practicePlayer1.setSpectatorItems();
        practicePlayer1.getPlayer().setAllowFlight(true);
        practicePlayer1.getPlayer().setFlying(true);
        practicePlayer1.getPlayer().sendMessage("§eSpectating §d" + practicePlayer2.getPlayer().getName()+"§e...");
        if(Account.getAccount(practicePlayer1.getPlayer()).getRank().getPower() < RankUnit.TRIALMOD.getPower()) {
            practicePlayer2.getPlayer().sendMessage("§6"+practicePlayer1.getPlayer().getName() + " §eis spectating dour match.");
            MatchManager.getEnemy(practicePlayer2).getPlayer().sendMessage("§6"+practicePlayer1.getPlayer().getName() + " §eis spectating dour match.");
        }
        practicePlayer1.hideAllPlayers(practicePlayer2, true);
    }

    public static void removeSpectator(PracticePlayer practicePlayer) {
        spectators.remove(practicePlayer);
    }

    public static boolean isSpectating(PracticePlayer practicePlayer) {
        return spectators.containsKey(practicePlayer);
    }

    public static HashMap<PracticePlayer, PracticePlayer> getSpectators() {
        return spectators;
    }
}
